<?php


use BusinessLogic\Models\Binding\UserBindingModel;
use BusinessLogic\Services\UserService;
use Configs\DatabaseConfig;
use PHPUnit\Framework\TestCase;

class UserServiceTest extends TestCase
{
    const PHONE = '+71234567890';
    const NAME = 'Somebody';
    const PASSWORD = 'abcdef12';
    private static PDO $pdo;
    private static UserService $service;

    public static function setUpBeforeClass(): void
    {
        self::$pdo = new PDO(
            DatabaseConfig::$dsn,
            DatabaseConfig::$userName,
            DatabaseConfig::$password
        );
        self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        self::$service = new UserService();
        self::$service->createUser(
            new UserBindingModel(
                self::PHONE,
                self::NAME,
                self::PASSWORD
            )
        );
    }


    public function testFailedLoginReturnsFalse()
    {
        $result = self::$service->login(self::PHONE, '1234');
        self::assertFalse($result);
    }


    public function testUserTokenWasNotSet()
    {
        $phone = self::PHONE;
        $userRecord = self::$pdo
            ->query("SELECT token, token_expires_at FROM users WHERE phone=$phone")
            ->fetchAll()[0];
        self::assertNull($userRecord['token']);
        self::assertNull($userRecord['token_expires_at']);
    }

    public static function tearDownAfterClass(): void
    {
        $phone = self::PHONE;
        self::$pdo->query("DELETE FROM users WHERE phone=$phone");
    }
}
