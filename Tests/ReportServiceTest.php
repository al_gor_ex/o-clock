<?php


use BusinessLogic\Services\ReportService;
use PHPUnit\Framework\TestCase;

class ReportServiceTest extends TestCase
{
    private ReportService $service;

    protected function setUp(): void
    {
        $this->service = new ReportService();
    }

    public function testMakeReportCreatesFile()
    {
        $dateFrom = '2021-06-25';
        $dateTo = '2021-07-13';
        $path = $this->service->makeReport($dateFrom, $dateTo);
        self::assertFileExists(__DIR__ . '/../storage/reports/' . $path);
    }
}
