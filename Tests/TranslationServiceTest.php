<?php


use BusinessLogic\Services\TranslationService;
use PHPUnit\Framework\TestCase;

class TranslationServiceTest extends TestCase
{

    public function testGetEngTransliteration()
    {
        $rusString = 'Съешь Же Ещё Этих Мягких Французских Булок Да Выпей Чаю';
        $expected = 'Sesh Zhe Eschyo Etih Myagkih Francuzskih Bulok Da Vypeij Chayu';
        $actual = TranslationService::getEngTransliteration($rusString);
        self::assertSame($expected, $actual);
    }
}
