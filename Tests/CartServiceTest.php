<?php


use BusinessLogic\Services\CartService;
use PHPUnit\Framework\TestCase;

class CartServiceTest extends TestCase
{
    private CartService $service;

    protected function setUp(): void
    {
        $this->service = new CartService();
    }

    public function testGetProductsListExceptionWhenCartIsEmpty()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Cart is empty');
        $result = $this->service->getProductsList('does_not_exist');
    }
}
