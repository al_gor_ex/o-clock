<?php

use BusinessLogic\Enums\Role;
use BusinessLogic\Guards\RoleGuard;
use BusinessLogic\Services\ProductService;

require_once __DIR__ . '/vendor/autoload.php';

(new RoleGuard())->letIn(Role::ADMIN);

if (!isset($_GET['id']) ||
    !isset($_GET['category-id'])) {
    die('Неверные параметры запроса');
}
$id = $_GET['id'];
$categoryId = $_GET['category-id'];

$productService = new ProductService();
$productService->deleteProduct($id);

// if called from Search page
if ($categoryId == -1) {
    header('Location: search.php');
} else {
    header("Location: catalog.php?category-id=$categoryId&page=1");
}
