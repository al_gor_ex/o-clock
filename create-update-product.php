<?php

use BusinessLogic\Enums\Role;
use BusinessLogic\Guards\RoleGuard;
use BusinessLogic\Models\Binding\ProductBindingModel;
use BusinessLogic\Services\ProductService;
use Intervention\Image\ImageManagerStatic;

require_once __DIR__ . '/vendor/autoload.php';

(new RoleGuard())->letIn(Role::ADMIN);

if (!isset($_REQUEST['id'])) {
    die('Неверные параметры запроса');
}
$id = $_REQUEST['id'];
$isNew = false;
if ($id == -1) {
    $id = null;
    $isNew = true;
}
$photoPath = null;
if (isset($_FILES['photo']) && $_FILES['photo']['name'] != '') {
    $uploadName = $_FILES['photo']['tmp_name'];
    $newName = 'storage/uploads/product/' . random_int(100_000, 1_000_000) . '.' .
        explode('.', $_FILES['photo']['name'])[1]; // file extension
    move_uploaded_file($uploadName, $newName);
    ImageManagerStatic::make($newName)->resize(500, 500)->save();
    $photoPath = $newName;
}
$categoryId = htmlspecialchars($_POST['category_id']);
$name = htmlspecialchars($_POST['name']);
$price = htmlspecialchars($_POST['price']);
$discountPercent = htmlspecialchars($_POST['discount_percent']);
$mechanismType = htmlspecialchars($_POST['mechanism_type']);
$countryName = htmlspecialchars($_POST['country_name']);
$availableCount = htmlspecialchars($_POST['available_count']);

$productService = new ProductService();
$model = new ProductBindingModel(
    $id,
    $photoPath,
    $categoryId,
    $name,
    $price,
    $discountPercent,
    $mechanismType,
    $countryName,
    $availableCount
);
if ($isNew) {
    $productService->createProduct($model);
} else {
    $productService->updateProduct($model);
}

header("Location: catalog.php?category-id=$categoryId&page=1");
