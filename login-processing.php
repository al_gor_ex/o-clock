<?php

use BusinessLogic\Enums\Role;
use BusinessLogic\Guards\RoleGuard;
use BusinessLogic\Services\UserService;

require_once __DIR__ . '/vendor/autoload.php';

(new RoleGuard())->letIn(Role::ANON);

$phone = null;
$password = null;
if (isset($_POST['phone'])) {
    $phone = htmlspecialchars($_POST['phone']);
}
if (isset($_POST['password'])) {
    $password = htmlspecialchars($_POST['password']);
}
if (is_null($phone) || is_null($password)) {
    die("Неверные параметры запроса");
}
$userService = new UserService();
$loginResult = $userService->login($phone, $password);

$smarty = new Smarty();
$smarty->assign('result', $loginResult);
$smarty->display('login-processing.tpl');
