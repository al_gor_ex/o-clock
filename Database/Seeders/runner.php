<?php

// Call this file to run all seeders placed in this folder
// Don't forget to setup your database credentials in DatabaseConfig.php file

use Configs\DatabaseConfig;

require_once __DIR__ . '/../../vendor/autoload.php';

function getRandomImgPath(): string
{
    return 'Database/Seeders/img/' . random_int(1, 3) . '.jpg';
}

$seederScripts = [];
$curDir = getcwd();
$dirDescr = opendir($curDir);
while (($file = readdir($dirDescr)) !== false) {
    $seederScripts[] = $file;
}
// Remove runner.php and non-php files
$seederScripts = array_filter(
    $seederScripts,
    fn($x) => strpos($x, '.php') !== false &&
        strpos($x, 'runner') === false
);

echo 'Found ' . count($seederScripts) . ' seeders';
echo '<br>';
echo 'BEGIN';

$pdo = new PDO(
    DatabaseConfig::$dsn,
    DatabaseConfig::$userName,
    DatabaseConfig::$password
);

foreach ($seederScripts as $seeder) {
    echo '<br>';
    echo 'Running seeder: ' . $seeder;
    $code = file_get_contents($seeder);
    eval($code);
    echo '<br>';
    echo 'Ok';
}

echo '<br>';
echo 'Database has been seeded successfully';
