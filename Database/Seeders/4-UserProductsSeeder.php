

require_once __DIR__ . '/../../vendor/autoload.php';

// Values will be pulled from these arrays to prevent repeating phone<->id combinations in database table.
$productIds = array_column($pdo->query('SELECT id FROM products')->fetchAll(), 'id');
$phones = array_column($pdo->query('SELECT phone FROM users')->fetchAll(), 'phone');

function getRandomProductId(PDO $pdo, array &$ids): int
{
    $randIndex = random_int(0, count($ids) - 1);
    $result = $ids[$randIndex];
    array_splice($ids, $randIndex, 1);
    return $result;
}

function getRandomUserPhone(PDO $pdo, array &$phones): string
{
    $randIndex = random_int(0, count($phones) - 1);
    $result = $phones[$randIndex];
    array_splice($phones, $randIndex, 1);
    return $result;
}

for ($i = 0; $i < 3; $i++) {
    $userPhone = getRandomUserPhone($pdo, $phones);
    for ($j = 0; $j < 10; $j++) {
        $productId = getRandomProductId($pdo, $productIds);
        $count = random_int(1, 3);
        $query = "INSERT INTO user_products (product_id, user_phone, count) VALUES " .
            "($productId, '$userPhone', $count)";
        $pdo->exec($query);
    }
}
