

use BusinessLogic\Enums\MechanismType;

require_once __DIR__ . '/../../vendor/autoload.php';

function getRandomMechanismType()
{
    $faker = Faker\Factory::create();
    $types = [MechanismType::MECHANIC, MechanismType::ELECTRONIC];
    return $faker->randomElement($types);
}

function getRandomCategoryId(PDO $pdo): int
{
    $faker = Faker\Factory::create();
    $ids = $pdo->query('SELECT id FROM categories')->fetchAll();
    return $faker->randomElement($ids)['id'];
}

$faker = Faker\Factory::create();

for ($i = 0; $i < 30; $i++) {
    $imgPath = getRandomImgPath();
    $price = random_int(500, 50_000);
    $discount = random_int(0, 10);
    $type = getRandomMechanismType();
    $country = Faker\Provider\ru_RU\Address::country();
    $engCountry = Faker\Provider\en_US\Address::country();
    $categoryId = getRandomCategoryId($pdo);

    $query = "INSERT INTO products (name, photo_path, price, discount_percent, " .
        "mechanism_type, country_name, country_eng_name, available_count, category_id) " .
        " VALUES ( '$faker->word', '$imgPath', $price, $discount," .
        " '$type', '$country', '$engCountry', $faker->randomDigit, $categoryId)";
    $pdo->exec($query);
}
