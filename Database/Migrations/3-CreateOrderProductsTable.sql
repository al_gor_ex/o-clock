use `o-clock`;
CREATE TABLE IF NOT EXISTS order_products
(
    product_id INT NOT NULL,
    order_id INT NOT NULL,
    count INT NOT NULL,
    PRIMARY KEY (product_id, order_id)
)
