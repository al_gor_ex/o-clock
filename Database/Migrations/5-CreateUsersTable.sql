use `o-clock`;
CREATE TABLE IF NOT EXISTS users
(
    phone VARCHAR(255) PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    created_at TIMESTAMP NOT NULL,
    token VARCHAR(255) NULL,
    token_expires_at TIMESTAMP NULL
)
