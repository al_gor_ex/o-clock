use `o-clock`;
CREATE TABLE IF NOT EXISTS orders
(
    id INT PRIMARY KEY AUTO_INCREMENT,
    created_at TIMESTAMP NOT NULL,
    sum FLOAT(10, 2) NOT NULL,
    json_description JSON NOT NULL,
    user_phone VARCHAR(255) NULL,
    FOREIGN KEY (user_phone) REFERENCES users(phone) ON DELETE SET NULL ON UPDATE CASCADE
)
