use `o-clock`;
CREATE TABLE IF NOT EXISTS user_products
(
    product_id INT NOT NULL,
    user_phone VARCHAR(255) NOT NULL,
    count INT NOT NULL,
    PRIMARY KEY (product_id, user_phone)
)
