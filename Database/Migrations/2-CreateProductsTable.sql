use `o-clock`;
CREATE TABLE IF NOT EXISTS products
(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    photo_path VARCHAR(255) NOT NULL,
    price INT NOT NULL,
    discount_percent INT NOT NULL,
    mechanism_type VARCHAR(255) NOT NULL,
    country_name VARCHAR(255) NOT NULL,
    country_eng_name VARCHAR(255) NOT NULL,
    available_count INT NOT NULL,
    category_id INT,
    FOREIGN KEY (category_id) REFERENCES categories (id) ON UPDATE CASCADE ON DELETE CASCADE
)
