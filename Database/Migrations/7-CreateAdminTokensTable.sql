use `o-clock`;
CREATE TABLE IF NOT EXISTS admin_tokens
(
    token VARCHAR(255) PRIMARY KEY,
    expires_at TIMESTAMP NOT NULL
)
