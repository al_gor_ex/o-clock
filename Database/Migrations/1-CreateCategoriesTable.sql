use `o-clock`;
CREATE TABLE IF NOT EXISTS categories
(
    id INT PRIMARY KEY AUTO_INCREMENT,
    photo_path VARCHAR(255) NOT NULL,
    name VARCHAR(255) NOT NULL
)
