<?php

// Call this file to run all sql-migrations placed in this folder
// Don't forget to setup your database credentials in DatabaseConfig.php file

use Configs\DatabaseConfig;

require_once __DIR__ . '/../../vendor/autoload.php';

$migrationFiles = [];
$curDir = getcwd();
$dirDescr = opendir($curDir);
while (($file = readdir($dirDescr)) !== false) {
    $migrationFiles[] = $file;
}
// filter non-sql files
$migrationFiles = array_filter($migrationFiles, fn($x) => strpos($x, '.sql') !== false);
echo 'Found ' . count($migrationFiles) . ' sql files';
echo '<br>';
echo 'BEGIN';
$pdo = new PDO(
    DatabaseConfig::$dsn,
    DatabaseConfig::$userName,
    DatabaseConfig::$password
);
foreach ($migrationFiles as $file) {
    $query = file_get_contents($file);
    echo '<br>';
    echo 'Applying migration: ' . $file;
    $pdo->exec($query);
    echo '<br>';
    echo 'Ok';
}
echo '<br>';
echo 'All migrations have been run successfully';
