<?php

use BusinessLogic\Enums\Role;
use BusinessLogic\Guards\RoleGuard;
use BusinessLogic\Services\OrderService;

require_once __DIR__ . '/vendor/autoload.php';
require_once 'navbar.php';

(new RoleGuard())->letIn(Role::USER_OR_ADMIN);

$phone = null;
if (isset($_COOKIE['phone'])) {
    $phone = htmlspecialchars($_COOKIE['phone']);
}
$page = 1;
if (isset($_GET['page'])) {
    $page = htmlspecialchars($_GET['page']);
}
if ($page < 1) {
    $page = 1;
}

$orderService = new OrderService();
$ordersList = $orderService->getOrdersList($page, $phone);
$orders = $ordersList->orders;

$pagesCount = $ordersList->totalPagesCount;
if ($page > $pagesCount) {
    $page = $pagesCount;
}

$smarty = new Smarty();
require_once 'navbar-assigns.php';
$smarty->assign('url', basename(__FILE__));
$smarty->assign('orders', $orders);
$smarty->assign('pagesCount', $pagesCount);
$smarty->assign('page', $page);
$smarty->display('orders.tpl');
