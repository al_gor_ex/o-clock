<?php

use BusinessLogic\Enums\Role;
use BusinessLogic\Guards\RoleGuard;
use BusinessLogic\Services\UserService;

require_once __DIR__ . '/vendor/autoload.php';
require_once 'navbar.php';

(new RoleGuard())->letIn(Role::ADMIN);

$userService = new UserService();
$accountsList = $userService->getAccountsList();

$smarty = new Smarty();
require_once 'navbar-assigns.php';
$smarty->assign('url', basename(__FILE__));
$smarty->assign('accountsList', $accountsList);
$smarty->display('accounts.tpl');
