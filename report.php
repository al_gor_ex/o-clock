<?php

use BusinessLogic\Enums\Role;
use BusinessLogic\Guards\RoleGuard;
use BusinessLogic\Services\ReportService;

require_once __DIR__ . '/vendor/autoload.php';

(new RoleGuard())->letIn(Role::ADMIN);

$dateFrom = null;
$dateTo = null;
if (isset($_GET['date-from'])) {
    $dateFrom = $_GET['date-from'];
}
if (isset($_GET['date-to'])) {
    $dateTo = $_GET['date-to'];
}
if (is_null($dateFrom) || is_null($dateTo)) {
    die('Неверные параметры запроса');
}

$reportService = new ReportService();
$fileName = $reportService->makeReport($dateFrom, $dateTo);

$smarty = new Smarty();
$smarty->assign('fileName', $fileName);
$smarty->display('report.tpl');
