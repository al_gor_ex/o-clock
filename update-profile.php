<?php

use BusinessLogic\Enums\Role;
use BusinessLogic\Guards\RoleGuard;
use BusinessLogic\Models\Binding\UserBindingModel;
use BusinessLogic\Services\UserService;

require_once __DIR__ . '/vendor/autoload.php';

(new RoleGuard())->letIn(Role::USER);

$phone = null;
$name = null;
$oldPassword = null;
$newPassword = null;

if (isset($_POST['phone'])) {
    $phone = htmlspecialchars($_POST['phone']);
}
if (isset($_POST['name'])) {
    $name = htmlspecialchars($_POST['name']);
}
if (isset($_POST['old-password'])) {
    $oldPassword = htmlspecialchars($_POST['old-password']);
}
if (isset($_POST['new-password'])) {
    $newPassword = htmlspecialchars($_POST['new-password']);
}

if (is_null($phone) || is_null($name) || is_null($oldPassword)) {
    die('Неверные параметры запроса');
}
$userService = new UserService();
$isVerified = $userService->verifyPassword(
    new UserBindingModel($phone, '', $oldPassword)
);
if ($isVerified) {
    $userService->updateUser(
        new UserBindingModel($phone, $name, $newPassword)
    );
}
$smarty = new Smarty();
$smarty->assign('result', $isVerified);
$smarty->display('update-profile.tpl');
