function onProductCountChange(productId, userPhone) {
    var newCount = Number(document.getElementById("productCount" + productId)
        .value);
    userPhone = encodeURIComponent(userPhone);
    location.href = "change-product-count.php?product=" + productId + "&phone=" + userPhone + "&count=" + newCount;
}
function confirmAndRedirectTo(productName, url) {
    if (confirm("\u0423\u0431\u0440\u0430\u0442\u044C \"" + productName + "\" \u0438\u0437 \u043A\u043E\u0440\u0437\u0438\u043D\u044B?")) {
        location.href = url;
    }
}
