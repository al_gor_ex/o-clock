enum DisplayType {
    PASSWORD = 'password',
    TEXT = 'text'
}

let passwordsInputType: string = DisplayType.PASSWORD

function togglePasswordsVisibility() {
    let form: HTMLFormElement = document.forms['credentials']
    let passwordInput = form['password']
    let passwordRepeatInput = form['repeat-password']
    passwordsInputType = (passwordsInputType === DisplayType.PASSWORD) ?
        DisplayType.TEXT :
        DisplayType.PASSWORD
    passwordInput.type = passwordsInputType
    passwordRepeatInput.type = passwordsInputType
}

function onLoad(): void {
    let form: HTMLFormElement = document.forms['credentials']
    form.addEventListener("submit", (event) => {
        if (!isInputValid()) {
            event.preventDefault()
        }
    })
    if (isPhoneOccupied()) {
        alert('Указанный номер телефона уже занят')
    }
}

function isPhoneOccupied(): boolean {
    return (window.location.search.search('phone-occupied=true') !== -1)
}

function displayResults(validationResults: boolean[]) {
    let errorMessages = document.getElementsByClassName('error-message')
    for (let i = 0; i < validationResults.length; i++) {
        let classList = errorMessages.item(i).classList
        if (!validationResults[i]) {
            classList.remove('hidden')
        } else {
            classList.add('hidden')
        }
    }
}

function isInputValid(): boolean {
    let form = document.forms['credentials']
    let validationResults: boolean[] = [
        isPhoneValid(form['phone'].value),
        isNameLengthValid(form['name'].value),
        isPasswordStrengthValid(form['password'].value),
        doPasswordsMatch(form['password'].value, form['repeat-password'].value)
    ]
    displayResults(validationResults)
    return (validationResults.indexOf(false) <= -1)
}

function isPhoneValid(phone: string): boolean {
    return (phone.search(/^((\+7)|8)[0-9]{10}$/g) !== -1)
}

function isNameLengthValid(name: string): boolean {
    return name.length <= 30
}

function isPasswordStrengthValid(password: string): boolean {
    let length = password.length;
    let numericSymbols = countOfNumericSymbols(password)
    return (
        length >= 8 &&
        numericSymbols >= 2 &&
        length - numericSymbols >= 6
    )
}

function doPasswordsMatch(password1: string, password2: string): boolean {
    return password1 === password2
}

function countOfNumericSymbols(password: string): number {
    let counter = 0
    for (let i = 0; i < password.length; i++) {
        let asciiCode = password[i].charCodeAt(0)
        if (asciiCode >= 48 && asciiCode <= 57) {
            counter++
        }
    }
    return counter
}
