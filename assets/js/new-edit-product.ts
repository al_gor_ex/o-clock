function onDiscountToggle() {
    let checkboxElement = document.getElementById('enable-discount') as HTMLInputElement
    let discountControls = document.getElementById('discount-controls')
    let discountPercentInput = document.querySelector('input[name="discount_percent"]') as HTMLInputElement
    if (checkboxElement.checked) {
        discountControls.classList.remove('hidden')
    } else {
        discountPercentInput.value = '0'
        onPriceOrPercentChange()
        discountControls.classList.add('hidden')
    }
}

function onPriceOrPercentChange() {
    let initialPriceInput = document.querySelector('input[name="price"]') as HTMLInputElement
    let discountPercentInput = document.querySelector('input[name="discount_percent"]') as HTMLInputElement
    let finalPriceField = document.getElementById('final-price')
    let priceBefore = parseInt(initialPriceInput.value)
    let discountPercent = parseInt(discountPercentInput.value)
    finalPriceField.innerText = (discountPercent === 0) ?
        '' :
        (priceBefore * (1 - discountPercent / 100)).toFixed(2)
}
