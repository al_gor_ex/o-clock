let minPrice;
let maxPrice;
function fadeFiltersArea() {
    $('.filters-area').fadeOut(750);
}

function initTooltips() {
    $(() => $('[data-toggle="tooltip"]').tooltip());
}

function initPriceRange() {
    minPrice = Number(document.querySelector('#min-price-label').innerText);
    maxPrice = Number(document.querySelector('#max-price-label').innerText);
    let cookieMinPrice = Cookies.get('minPrice');
    let cookieMaxPrice = Cookies.get('maxPrice');
    let selectedMinPrice;
    let selectedMaxPrice;
    if (cookieMinPrice !== undefined && cookieMaxPrice !== undefined) {
        selectedMinPrice = cookieMinPrice;
        selectedMaxPrice = cookieMaxPrice;
    } else {
        selectedMinPrice = minPrice;
        selectedMaxPrice = maxPrice;
    }
    $(() => {
        let id = "#price-range";
        $(id).slider({
            range: true,
            min: minPrice,
            max: maxPrice,
            values: [selectedMinPrice, selectedMaxPrice],
            slide: (event, ui) => {
                $("#min-price").val(ui.values[0]);
                $('#max-price').val(ui.values[1]);
            }
        });
    });
}

function onLoad() {
    initPriceRange();
    initTooltips();
}



function onBtnApplyFiltersClick() {
    unsetFiltersCookies();
    Cookies.set('sortBy', $('select#sort-by').val());
    Cookies.set('countryEngName', $('select#country-name').val());
    let mechanismValues = ['mech', 'elec'];
    let mechanismTypes = [];
    for (let value of mechanismValues) {
        if ($(`input[value=${value}]`).is(':checked')) {
            mechanismTypes.push(value);
        }
    }
    let mechanismTypesString = mechanismTypes.join(', ');
    Cookies.set('mechanismTypes', mechanismTypesString);
    Cookies.set('minPrice', $('input#min-price').val());
    Cookies.set('maxPrice', $('input#max-price').val());
    reloadPage();
}

function onBtnClearFiltersClick() {
    unsetFiltersCookies();
    reloadPage();
}

function unsetFiltersCookies() {
    Cookies.remove('countryEngName');
    Cookies.remove('sortBy');
    Cookies.remove('mechanismTypes');
    Cookies.remove('minPrice');
    Cookies.remove('maxPrice');
}

function reloadPage() {
    window.location.href = window.location.href;
}
