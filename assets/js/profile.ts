enum DisplayType {
    PASSWORD = 'password',
    TEXT = 'text'
}

let passwordsInputType: string = DisplayType.PASSWORD

function togglePasswordsVisibility() {
    let form: HTMLFormElement = document.forms['credentials']
    let oldPasswordInput = form['old-password']
    let newPasswordInput = form['new-password']
    let passwordRepeatInput = form['repeat-password']
    passwordsInputType = (passwordsInputType === DisplayType.PASSWORD) ?
        DisplayType.TEXT :
        DisplayType.PASSWORD
    oldPasswordInput.type = passwordsInputType
    newPasswordInput.type = passwordsInputType
    passwordRepeatInput.type = passwordsInputType
}

function onLoad(): void {
    let form: HTMLFormElement = document.forms['credentials']
    form.addEventListener("submit", (event) => {
        if (!isInputValid()) {
            event.preventDefault()
        }
    })
}

function displayResults(validationResults: boolean[]) {
    let errorMessages = document.getElementsByClassName('error-message')
    for (let i = 0; i < validationResults.length; i++) {
        let classList = errorMessages.item(i).classList
        if (!validationResults[i]) {
            classList.remove('hidden')
        } else {
            classList.add('hidden')
        }
    }
}

function isInputValid(): boolean {
    let form = document.forms['credentials']
    let validationResults: boolean[] = [
        isNameLengthValid(form['name'].value),
        isPasswordStrengthValid(form['new-password'].value),
        doPasswordsMatch(form['new-password'].value, form['repeat-password'].value)
    ]
    displayResults(validationResults)
    return (validationResults.indexOf(false) <= -1)
}

function isNameLengthValid(name: string): boolean {
    return name.length <= 30
}

function isPasswordStrengthValid(password: string): boolean {
    let length = password.length;
    let numericSymbols = countOfNumericSymbols(password)
    return (
        length == 0 ||
        (length >= 8 &&
        numericSymbols >= 2 &&
        length - numericSymbols >= 6)
    )
}

function doPasswordsMatch(password1: string, password2: string): boolean {
    return password1 === password2
}

function countOfNumericSymbols(password: string): number {
    let counter = 0
    for (let i = 0; i < password.length; i++) {
        let asciiCode = password[i].charCodeAt(0)
        if (asciiCode >= 48 && asciiCode <= 57) {
            counter++
        }
    }
    return counter
}
