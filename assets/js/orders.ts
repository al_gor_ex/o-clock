function onBtnShowFormClick() {
    let btn = document.getElementById('btn-show-export-form')
    let form = document.getElementById('export-form')
    btn.style.visibility = 'hidden'
    form.style.visibility = 'visible'
}

function onLoad() {
    let form: HTMLFormElement = document.forms['report-dates']
    form.addEventListener("submit", (event) => {
        if (!isInputValid(form)) {
            event.preventDefault()
        }
    })
}

function isInputValid(form: HTMLFormElement): boolean {
    let from = Date.parse(form['date-from'].value)
    let to = Date.parse(form['date-to'].value)
    if (from > to) {
        alert('Дата начала должна быть меньше даты окончания!')
        return false
    }
    return true
}
