var sequence = [0, 1, 2, 5, 8, 7, 6, 3];
var seqLen = sequence.length;
var seqI = 0;
function onLoad() {
    makeAnimationLoop();
}
function makeAnimationLoop() {
    var advantages = document.getElementsByClassName('advantage');
    var highlightIndex = sequence[(seqI + 1) % seqLen];
    advantages[highlightIndex].classList.add('advantage--highlighted');
    var darkenIndex = sequence[seqI % seqLen];
    advantages[darkenIndex].classList.remove('advantage--highlighted');
    seqI = ++seqI % seqLen;
    setTimeout(function () { return makeAnimationLoop(); }, 1250);
}
