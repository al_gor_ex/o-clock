var passwordsInputType = 'password';
var DisplayType;
(function (DisplayType) {
    DisplayType["PASSWORD"] = "password";
    DisplayType["TEXT"] = "text";
})(DisplayType || (DisplayType = {}));
function togglePasswordVisibility() {
    var form = document.forms['credentials'];
    var passwordInput = form['password'];
    passwordsInputType = (passwordsInputType === DisplayType.PASSWORD) ?
        DisplayType.TEXT :
        DisplayType.PASSWORD;
    passwordInput.type = passwordsInputType;
}
