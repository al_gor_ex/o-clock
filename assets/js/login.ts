enum DisplayType {
    PASSWORD = 'password',
    TEXT = 'text'
}

let passwordsInputType: string = DisplayType.PASSWORD

function togglePasswordVisibility() {
    let form: HTMLFormElement = document.forms['credentials']
    let passwordInput = form['password']
    passwordsInputType = (passwordsInputType === DisplayType.PASSWORD) ?
        DisplayType.TEXT :
        DisplayType.PASSWORD
    passwordInput.type = passwordsInputType
}
