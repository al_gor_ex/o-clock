function onDiscountToggle() {
    var checkboxElement = document.getElementById('enable-discount');
    var discountControls = document.getElementById('discount-controls');
    var discountPercentInput = document.querySelector('input[name="discount_percent"]');
    if (checkboxElement.checked) {
        discountControls.classList.remove('hidden');
    }
    else {
        discountPercentInput.value = '0';
        onPriceOrPercentChange();
        discountControls.classList.add('hidden');
    }
}
function onPriceOrPercentChange() {
    var initialPriceInput = document.querySelector('input[name="price"]');
    var discountPercentInput = document.querySelector('input[name="discount_percent"]');
    var finalPriceField = document.getElementById('final-price');
    var priceBefore = parseInt(initialPriceInput.value);
    var discountPercent = parseInt(discountPercentInput.value);
    finalPriceField.innerText = (discountPercent === 0) ?
        '' :
        (priceBefore * (1 - discountPercent / 100)).toFixed(2);
}
