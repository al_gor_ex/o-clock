ymaps.ready(init);

function init() {
    var myMap = new ymaps.Map("map", {
            center: [55.76, 37.585],
            zoom: 14.29
        }, {
            searchControlProvider: 'yandex#search'
        });

    myMap.geoObjects
        .add(new ymaps.Placemark([55.762746, 37.602405], {
            balloonContent: 'ул. Снежная, 8а'
        }, {
            preset: 'islands#dotIcon',
            iconColor: '#735184'
        }))
        .add(new ymaps.Placemark([55.752652, 37.594893], {
            balloonContent: 'Пр-д. Неоновый, 3'
        }, {
            preset: 'islands#greenDotIconWithCaption'
        }))
        .add(new ymaps.Placemark([55.760965, 37.571165], {
            balloonContent: 'Ул. Паскаля, 23'
        }, {
            preset: 'islands#redIcon'
        }));
}
