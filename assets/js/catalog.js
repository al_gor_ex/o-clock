function resizeProductsArea() {
    var productsArea = document.getElementsByClassName('products-area')[0];
    setTimeout(function () {
        productsArea.classList.replace('col-sm-9', 'col-sm-12');
        productsArea.classList.remove('products-area');
    }, 750);
}
function deleteProduct(id, categoryId) {
    if (confirm("Удалить товар?")) {
        window.location.href = "delete-product.php?id=" + id + "&category-id=" + categoryId;
    }
}
