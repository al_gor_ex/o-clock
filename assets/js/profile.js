var DisplayType;
(function (DisplayType) {
    DisplayType["PASSWORD"] = "password";
    DisplayType["TEXT"] = "text";
})(DisplayType || (DisplayType = {}));
var passwordsInputType = DisplayType.PASSWORD;
function togglePasswordsVisibility() {
    var form = document.forms['credentials'];
    var oldPasswordInput = form['old-password'];
    var newPasswordInput = form['new-password'];
    var passwordRepeatInput = form['repeat-password'];
    passwordsInputType = (passwordsInputType === DisplayType.PASSWORD) ?
        DisplayType.TEXT :
        DisplayType.PASSWORD;
    oldPasswordInput.type = passwordsInputType;
    newPasswordInput.type = passwordsInputType;
    passwordRepeatInput.type = passwordsInputType;
}
function onLoad() {
    var form = document.forms['credentials'];
    form.addEventListener("submit", function (event) {
        if (!isInputValid()) {
            event.preventDefault();
        }
    });
}
function displayResults(validationResults) {
    var errorMessages = document.getElementsByClassName('error-message');
    for (var i = 0; i < validationResults.length; i++) {
        var classList = errorMessages.item(i).classList;
        if (!validationResults[i]) {
            classList.remove('hidden');
        }
        else {
            classList.add('hidden');
        }
    }
}
function isInputValid() {
    var form = document.forms['credentials'];
    var validationResults = [
        isNameLengthValid(form['name'].value),
        isPasswordStrengthValid(form['new-password'].value),
        doPasswordsMatch(form['new-password'].value, form['repeat-password'].value)
    ];
    displayResults(validationResults);
    return (validationResults.indexOf(false) <= -1);
}
function isNameLengthValid(name) {
    return name.length <= 30;
}
function isPasswordStrengthValid(password) {
    var length = password.length;
    var numericSymbols = countOfNumericSymbols(password);
    return (length == 0 ||
        (length >= 8 &&
            numericSymbols >= 2 &&
            length - numericSymbols >= 6));
}
function doPasswordsMatch(password1, password2) {
    return password1 === password2;
}
function countOfNumericSymbols(password) {
    var counter = 0;
    for (var i = 0; i < password.length; i++) {
        var asciiCode = password[i].charCodeAt(0);
        if (asciiCode >= 48 && asciiCode <= 57) {
            counter++;
        }
    }
    return counter;
}
