function onBtnShowFormClick() {
    var btn = document.getElementById('btn-show-export-form');
    var form = document.getElementById('export-form');
    btn.style.visibility = 'hidden';
    form.style.visibility = 'visible';
}
function onLoad() {
    var form = document.forms['report-dates'];
    form.addEventListener("submit", function (event) {
        if (!isInputValid(form)) {
            event.preventDefault();
        }
    });
}
function isInputValid(form) {
    var from = Date.parse(form['date-from'].value);
    var to = Date.parse(form['date-to'].value);
    if (from > to) {
        alert('Дата начала должна быть меньше даты окончания!');
        return false;
    }
    return true;
}
