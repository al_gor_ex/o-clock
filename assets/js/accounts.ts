function deleteAccount(phone: string) {
    phone = encodeURIComponent(phone)
    if (confirm('Удалить пользователя?')) {
        window.location.href = `delete-user.php?phone=${phone}`
    }
}
