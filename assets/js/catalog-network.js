function notifyCart(productId, operation) {
    if (operation === 'remove') {
        if (confirm('Вы действительно хотите убрать товар из корзины?') === false) {
            return;
        }
    }
    let userPhone = Cookies.get('phone');
    let oldButtonId = `${operation}${productId}`;
    let oldButtonSelector = `#${operation}${productId}`;
    let buttonParent = $(oldButtonSelector).parent();
    // Remove button
    $(oldButtonSelector).remove();
    // Add 'loading' icon
    let buttonParentHtml = buttonParent.html();
    buttonParent.html(
        buttonParentHtml +
        `<img id="${oldButtonId}" src="assets/img/icons/loading.gif">`
    );
    // Send query
    let token = Cookies.get('token');
    let dataToSend = {
        operation: operation,
        userPhone: userPhone,
        productId: productId
    };
    let newButtonHtml = ``;
    if (operation === 'add') {
        newButtonHtml = `<div class="btn btn-danger w-100" id="remove${productId}" 
            onclick="notifyCart(${productId}, 'remove')">
                            Убрать из корзины
                        </div>`;
    } else if (operation === 'remove') {
        newButtonHtml = `<div class="btn btn-primary w-100" id="add${productId}" 
            onclick="notifyCart(${productId}, 'add')">
                            В корзину
                        </div>`;
    }
    $.ajax(
        'BusinessLogic/Controllers/cart-controller.php',
        {
            type: 'POST',
            data: JSON.stringify(dataToSend),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {"Authorization": `Bearer ${token}`},
            error: (errMsg) => {
                if (errMsg.status === 200) {
                    // Replace 'loading' icon with new button
                    buttonParent.html(
                        buttonParentHtml +
                        newButtonHtml
                    );
                } else {
                    // Redirect to private zone to trigger RoleGuard
                    document.location.href = 'cart.php';
                }
            }
        }
    );
}
