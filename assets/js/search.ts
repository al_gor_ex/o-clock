function deleteProduct(id: number, categoryId: number) {
    if (confirm("Удалить товар?")) {
        window.location.href = `delete-product.php?id=${id}&category-id=${categoryId}`;
    }
}
