function resizeProductsArea() {
    let productsArea = document.getElementsByClassName('products-area')[0]
    setTimeout(() => {
        productsArea.classList.replace('col-sm-9', 'col-sm-12')
        productsArea.classList.remove('products-area')
    }, 750);
}

function deleteProduct(id: number, categoryId: number) {
    if (confirm("Удалить товар?")) {
        window.location.href = `delete-product.php?id=${id}&category-id=${categoryId}`;
    }
}
