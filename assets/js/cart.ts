function onProductCountChange(productId: number, userPhone: string) {
    let newCount: number = Number(
        (document.getElementById(`productCount${productId}`) as HTMLInputElement)
            .value
    );
    userPhone = encodeURIComponent(userPhone);
    location.href = `change-product-count.php?product=${productId}&phone=${userPhone}&count=${newCount}`;
}

function confirmAndRedirectTo(productName: string, url: string) {
    if (confirm(`Убрать "${productName}" из корзины?`)) {
        location.href = url;
    }
}