let sequence = [0, 1, 2, 5, 8, 7, 6, 3]
let seqLen = sequence.length
let seqI = 0

function onLoad(): void {
    makeAnimationLoop()
}

function makeAnimationLoop(): void {
    let advantages: HTMLCollectionOf<Element> = document.getElementsByClassName('advantage')
    let highlightIndex = sequence[(seqI + 1) % seqLen]
    advantages[highlightIndex].classList.add('advantage--highlighted')
    let darkenIndex = sequence[seqI % seqLen]
    advantages[darkenIndex].classList.remove('advantage--highlighted')
    seqI = ++seqI % seqLen
    setTimeout(() => makeAnimationLoop(), 1_250)
}
