<?php

use BusinessLogic\Services\CategoryService;
use BusinessLogic\Services\ProductService;

require_once __DIR__ . '/vendor/autoload.php';
require_once 'navbar.php';

$phrase = null;
$categoryId = null;
$userPhone = null;
if (isset($_GET['phrase'])) {
    $phrase = htmlspecialchars($_GET['phrase']);
}
if (isset($_GET['category-id'])) {
    $categoryId = htmlspecialchars($_GET['category-id']);
}
if (isset($_COOKIE['phone'])) {
    $userPhone = htmlspecialchars($_COOKIE['phone']);
}

$categoryService = new CategoryService();
$categories = $categoryService->getCategoriesList();
$productService = new ProductService();
$productsList = null;
if (!is_null($phrase)) {
    $productsList = $productService->getSearchResult($phrase, $categoryId, $userPhone);
}

$smarty = new Smarty();
require_once 'navbar-assigns.php';
$smarty->assign('phrase', $phrase);
$smarty->assign('categories', $categories);
$smarty->assign('categoryId', $categoryId);
$smarty->assign('productsList', $productsList);
$smarty->assign('url', basename(__FILE__));
$smarty->display('search.tpl');
