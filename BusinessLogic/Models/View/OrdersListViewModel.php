<?php


namespace BusinessLogic\Models\View;


class OrdersListViewModel
{
    /**
     * @var OrderViewModel[]
     */
    public array $orders;
    public int $totalPagesCount;

    /**
     * @param OrderViewModel[] $orders
     */
    public function __construct(array $orders, int $totalPagesCount)
    {
        $this->orders = $orders;
        $this->totalPagesCount = $totalPagesCount;
    }
}
