<?php


namespace BusinessLogic\Models\View;


class CartViewModel
{
    public int $productsCount;
    public string $price;
    public string $discountSum;
    public string $finalPrice;
    /**
     * @var CartProductViewModel[]
     */
    public array $products;

    /**
     * @param CartProductViewModel[] $products
     */
    public function __construct(
        int $productsCount, string $price, string $discountSum,
        string $finalPrice, array $products
    )
    {
        $this->productsCount = $productsCount;
        $this->price = $price;
        $this->discountSum = $discountSum;
        $this->finalPrice = $finalPrice;
        $this->products = $products;
    }
}
