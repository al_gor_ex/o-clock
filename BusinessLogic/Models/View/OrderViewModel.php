<?php


namespace BusinessLogic\Models\View;


class OrderViewModel
{
    public string $id;
    public string $createdAt;
    public string $sum;
    public string $userPhone;
    public string $userName;
    /**
     * @var OrderProductViewModel[]
     */
    public array $products;


    /**
     * @param OrderProductViewModel[] $products
     */
    public function __construct(
        string $id, string $createdAt, string $sum,
        string $userPhone, string $userName, array $products
    )
    {
        $this->id = $id;
        $this->createdAt = $createdAt;
        $this->sum = $sum;
        $this->userPhone = $userPhone;
        $this->userName = $userName;
        $this->products = $products;
    }
}
