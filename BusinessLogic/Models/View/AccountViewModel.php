<?php


namespace BusinessLogic\Models\View;


class AccountViewModel
{
    public string $phone;
    public string $name;
    public string $registrationDate;
    public int $ordersCount;
    public ?string $averageSum;
    public ?string $lastOrderDate;

    public function __construct(
        string $phone, string $name, string $registrationDate,
        int $ordersCount, ?string $averageSum, ?string $lastOrderDate
    )
    {
        $this->phone = $phone;
        $this->name = $name;
        $this->registrationDate = $registrationDate;
        $this->ordersCount = $ordersCount;
        $this->averageSum = $averageSum;
        $this->lastOrderDate = $lastOrderDate;
    }
}
