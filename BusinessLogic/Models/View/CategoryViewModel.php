<?php


namespace BusinessLogic\Models\View;


class CategoryViewModel
{
    public int $id;
    public string $name;
    public string $photoPath;

    public function __construct(int $id, string $name, string $photoPath)
    {
        $this->id = $id;
        $this->name = $name;
        $this->photoPath = $photoPath;
    }
}
