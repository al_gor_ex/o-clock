<?php


namespace BusinessLogic\Models\View;


class CategoryEditorViewModel
{
    public string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }
}
