<?php


namespace BusinessLogic\Models\View;


class ProductViewModel
{
    public int $id;
    public string $photoPath;
    public string $name;
    public int $price;
    public int $discountPercent;
    public ?string $finalPrice;
    public string $mechanismType;
    public string $countryName;
    public int $availableCount;
    public int $soldCount;
    public bool $wasAddedToCart;
    public int $totalPagesCount;

    public function __construct(
        int $id, string $photoPath, string $name, int $price,
        int $discountPercent, ?string $finalPrice, string $mechanismType,
        string $countryName, int $availableCount, int $soldCount, bool $wasAddedToCart,
        int $totalPagesCount
    )
    {
        $this->id = $id;
        $this->photoPath = $photoPath;
        $this->name = $name;
        $this->price = $price;
        $this->discountPercent = $discountPercent;
        $this->finalPrice = $finalPrice;
        $this->mechanismType = $mechanismType;
        $this->countryName = $countryName;
        $this->availableCount = $availableCount;
        $this->soldCount = $soldCount;
        $this->wasAddedToCart = $wasAddedToCart;
        $this->totalPagesCount = $totalPagesCount;
    }
}
