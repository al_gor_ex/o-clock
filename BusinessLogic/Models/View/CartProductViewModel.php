<?php


namespace BusinessLogic\Models\View;


class CartProductViewModel
{
    public int $id;
    public string $name;
    public string $photoPath;
    public string $price;
    public int $discountPercent;
    public string $finalPrice;
    public string $count;
    public string $finalSum;

    public function __construct(
        int $id, string $name, string $photoPath, string $price,
        int $discountPercent, string $finalPrice,
        string $count, string $finalSum
    )
    {
        $this->id = $id;
        $this->name = $name;
        $this->photoPath = $photoPath;
        $this->price = $price;
        $this->discountPercent = $discountPercent;
        $this->finalPrice = $finalPrice;
        $this->count = $count;
        $this->finalSum = $finalSum;
    }
}
