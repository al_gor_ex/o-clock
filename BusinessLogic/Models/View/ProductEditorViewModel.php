<?php


namespace BusinessLogic\Models\View;


class ProductEditorViewModel
{
    public int $categoryId;
    public string $name;
    public int $price;
    public int $discountPercent;
    public string $mechanismType;
    public string $countryName;
    public int $availableCount;


    public function __construct(int $categoryId, string $name, int $price, int $discountPercent, string $mechanismType, string $countryName, int $availableCount)
    {
        $this->categoryId = $categoryId;
        $this->name = $name;
        $this->price = $price;
        $this->discountPercent = $discountPercent;
        $this->mechanismType = $mechanismType;
        $this->countryName = $countryName;
        $this->availableCount = $availableCount;
    }
}
