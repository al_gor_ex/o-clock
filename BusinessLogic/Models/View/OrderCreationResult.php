<?php


namespace BusinessLogic\Models\View;


class OrderCreationResult
{
    public bool $isSuccessful;
    /**
     * @var string[]
     */
    public array $unavailableNames;


    /**
     * @param string[] $unavailableNames
     */
    public function __construct(bool $isSuccessful, array $unavailableNames = [])
    {
        $this->isSuccessful = $isSuccessful;
        $this->unavailableNames = $unavailableNames;
    }
}
