<?php


namespace BusinessLogic\Models\View;


class OrderProductViewModel
{
    public string $name;
    public string $finalPrice;
    public int $count;
    public string $sum;

    public function __construct(string $name, string $finalPrice, int $count, string $sum)
    {
        $this->name = $name;
        $this->finalPrice = $finalPrice;
        $this->count = $count;
        $this->sum = $sum;
    }
}
