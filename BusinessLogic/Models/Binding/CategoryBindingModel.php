<?php


namespace BusinessLogic\Models\Binding;


class CategoryBindingModel
{
    public ?int $id;
    public ?string $photoPath;
    public string $name;

    public function __construct(?int $id, ?string $photoPath, string $name)
    {
        $this->id = $id;
        $this->photoPath = $photoPath;
        $this->name = $name;
    }
}
