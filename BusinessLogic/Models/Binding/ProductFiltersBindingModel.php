<?php


namespace BusinessLogic\Models\Binding;


class ProductFiltersBindingModel
{
    public int $categoryId;
    public ?string $userPhone;
    public ?string $sortingType;
    public ?string $countryEngName;
    public ?string $mechanismType;
    public ?int $minPrice;
    public ?int $maxPrice;
    public int $pageNumber;

    public function __construct(
        int $categoryId, ?string $userPhone, ?string $sortingType, ?string $countryEngName,
        ?string $mechanismType, ?int $minPrice, ?int $maxPrice, int $pageNumber
    )
    {
        $this->categoryId = $categoryId;
        $this->userPhone = $userPhone;
        $this->sortingType = $sortingType;
        $this->countryEngName = $countryEngName;
        $this->mechanismType = $mechanismType;
        $this->minPrice = $minPrice;
        $this->maxPrice = $maxPrice;
        $this->pageNumber = $pageNumber;
    }

}
