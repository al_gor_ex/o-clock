<?php


namespace BusinessLogic\Models\Binding;


class ProductBindingModel
{
    public ?int $id;
    public ?string $photoPath;
    public string $categoryId;
    public string $name;
    public int $price;
    public int $discountPercent;
    public string $mechanismType;
    public string $countryName;
    public int $availableCount;

    public function __construct(
        ?int $id, ?string $photoPath, string $categoryId,
        string $name, int $price, int $discountPercent,
        string $mechanismType, string $countryName, int $availableCount
    )
    {
        $this->id = $id;
        $this->photoPath = $photoPath;
        $this->categoryId = $categoryId;
        $this->name = $name;
        $this->price = $price;
        $this->discountPercent = $discountPercent;
        $this->mechanismType = $mechanismType;
        $this->countryName = $countryName;
        $this->availableCount = $availableCount;
    }
}
