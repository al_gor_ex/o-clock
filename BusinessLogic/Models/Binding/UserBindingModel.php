<?php


namespace BusinessLogic\Models\Binding;


class UserBindingModel
{
    public string $phone;
    public string $name;
    public string $password;

    public function __construct(string $phone, string $name, string $password)
    {
        $this->phone = $phone;
        $this->name = $name;
        $this->password = $password;
    }
}
