<?php


namespace BusinessLogic\Enums;


abstract class GuardResult
{
    const OK = 'ok';
    const WRONG = 'wrong';
    const OUTDATED = 'outdated';
}
