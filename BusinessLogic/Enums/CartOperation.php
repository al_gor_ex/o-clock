<?php


namespace BusinessLogic\Enums;


abstract class CartOperation
{
    const ADD = 'add';
    const REMOVE = 'remove';
    const SET = 'set';
}
