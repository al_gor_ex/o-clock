<?php


namespace BusinessLogic\Enums;


abstract class Role
{
    const ANON = 'anon';
    const USER = 'user';
    const ADMIN = 'admin';
    const USER_OR_ADMIN = 'usad';
}
