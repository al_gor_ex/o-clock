<?php


namespace BusinessLogic\Enums;


abstract class SortingType
{
    const POPULARITY = 'popularity';
    const NAME = 'name';
    const PRICE_ASC = 'priceAsc';
    const PRICE_DESC = 'priceDesc';
    const DISCOUNT = 'discount';
}
