<?php

namespace BusinessLogic\Enums;

abstract class MechanismType
{
    const MECHANIC = 'mech';
    const ELECTRONIC = 'elec';
}
