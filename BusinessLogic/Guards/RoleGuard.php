<?php


namespace BusinessLogic\Guards;


use BusinessLogic\Enums\GuardResult;
use BusinessLogic\Enums\Role;
use Carbon\Carbon;
use Configs\DatabaseConfig;
use PDO;

class RoleGuard
{
    private PDO $pdo;

    public function __construct()
    {
        $this->pdo = new PDO(
            DatabaseConfig::$dsn,
            DatabaseConfig::$userName,
            DatabaseConfig::$password
        );
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * Restricts access to the current page for all roles, except spicified
     * @param string $role Allowed role (BusinessLogic\Enums\Role constant)
     */
    public function letIn(string $role): void
    {
        switch ($role) {
            case Role::ANON:
                if ($this->verifyAnon() == false) {
                    $this->redirectTo('home.php');
                }
                break;
            case Role::USER:
                $result = $this->verifyUser();
                if ($result == GuardResult::OUTDATED) {
                    $updateQuery = 'UPDATE users SET token=NULL, token_expires_at=NULL WHERE token=?';
                    $updateQuery = $this->pdo->prepare($updateQuery);
                    $updateQuery->execute([htmlspecialchars($_COOKIE['token'])]);
                }
                if ($result == GuardResult::WRONG || $result == GuardResult::OUTDATED) {
                    $this->unsetCookies();
                    $this->redirectTo('login.php');
                }
                break;
            case Role::ADMIN:
                $result = $this->verifyAdmin();
                if ($result == GuardResult::OUTDATED) {
                    $query = 'DELETE FROM admin_tokens';
                    $this->pdo->exec($query);
                }
                if ($result == GuardResult::WRONG || $result == GuardResult::OUTDATED) {
                    $this->unsetCookies();
                    $this->redirectTo('admin-login.html');
                }
                break;
            case Role::USER_OR_ADMIN:
                if ($this->verifyUser() != GuardResult::OK &&
                    $this->verifyAdmin() != GuardResult::OK) {
                    $this->unsetCookies();
                    $this->redirectTo('login.php');
                }
                break;
        }
    }

    private function verifyAnon(): bool
    {
        return (
                isset($_COOKIE['role']) ||
                isset($_COOKIE['token'])
            ) == false;
    }

    /**
     * @return string Result (BusinessLogic\Enums\GuardResult constant)
     */
    private function verifyUser(): string
    {
        if (!isset($_COOKIE['role']) ||
            !isset($_COOKIE['token']) ||
            !isset($_COOKIE['phone'])) {
            return GuardResult::WRONG;
        }
        $token = htmlspecialchars($_COOKIE['token']);
        $userName = htmlspecialchars($_COOKIE['userName']);
        $query = 'SELECT * FROM users WHERE token=? AND name=?';
        $query = $this->pdo->prepare($query);
        $query->execute([$token, $userName]);
        $users = $query->fetchAll();
        if (empty($users)) {
            return GuardResult::WRONG;
        }
        $user = $users[0];
        $crb = Carbon::createFromTimeString($user['token_expires_at']);
        if ($crb < Carbon::now()) {
            return GuardResult::OUTDATED;
        }
        return GuardResult::OK;
    }

    /**
     * @return string Result (BusinessLogic\Enums\GuardResult constant)
     */
    private function verifyAdmin(): string
    {
        if (!isset($_COOKIE['role']) || !isset($_COOKIE['token'])) {
            return GuardResult::WRONG;
        }
        $token = htmlspecialchars($_COOKIE['token']);
        $query = 'SELECT * FROM admin_tokens';
        $adminTokens = $this->pdo->query($query)->fetchAll();
        if (empty($adminTokens)) {
            return GuardResult::WRONG;
        }
        $adminToken = $adminTokens[0];
        if ($adminToken['token'] != $token) {
            return GuardResult::WRONG;
        }
        $crb = Carbon::createFromTimeString($adminToken['expires_at']);
        if ($crb < Carbon::now()) {
            return GuardResult::OUTDATED;
        }
        return GuardResult::OK;
    }

    private function redirectTo(string $page): void
    {
        header("Location: $page");
        exit();
    }

    private function unsetCookies(): void
    {
        $cookieNames = ['role', 'token', 'userName', 'phone'];
        foreach ($cookieNames as $name) {
            setcookie($name, '', time() - 1);
        }
    }
}
