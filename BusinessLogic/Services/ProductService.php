<?php


namespace BusinessLogic\Services;


use BusinessLogic\Enums\SortingType;
use BusinessLogic\Models\Binding\ProductBindingModel;
use BusinessLogic\Models\Binding\ProductFiltersBindingModel;
use BusinessLogic\Models\View\ProductEditorViewModel;
use BusinessLogic\Models\View\ProductViewModel;
use Configs\CatalogConfig;
use Configs\DatabaseConfig;
use Exception;
use PDO;

class ProductService
{
    private PDO $pdo;

    public function __construct()
    {
        $this->pdo = new PDO(
            DatabaseConfig::$dsn,
            DatabaseConfig::$userName,
            DatabaseConfig::$password
        );
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function getPriceRange($categoryId): array
    {
        $query = "SELECT count(*) FROM products WHERE category_id=?;";
        $query = $this->pdo->prepare($query);
        $query->execute([$categoryId]);
        $isEmpty = ($query->fetchAll()[0][0] == 0);
        if ($isEmpty) {
            return ['lower' => 0, 'upper' => 1];
        }
        $queryLower = "SELECT price FROM products WHERE category_id=? ORDER BY price LIMIT 1;";
        $queryUpper = "SELECT price FROM products WHERE category_id=? ORDER BY price DESC LIMIT 1;";
        $queryLower = $this->pdo->prepare($queryLower);
        $queryUpper = $this->pdo->prepare($queryUpper);
        $queryLower->execute([$categoryId]);
        $queryUpper->execute([$categoryId]);
        $lowerBound = $queryLower->fetchAll()[0][0];
        $upperBound = $queryUpper->fetchAll()[0][0];
        return [
            'lower' => $lowerBound,
            'upper' => $upperBound
        ];
    }

    public function getCountriesList($categoryId): array
    {
        $query = "SELECT DISTINCT country_name AS name, country_eng_name AS value FROM products " .
            "WHERE category_id=? ORDER BY country_name;";
        $query = $this->pdo->prepare($query);
        $query->execute([$categoryId]);
        return $query->fetchAll();
    }

    public function getProductForEditing(int $id): ProductEditorViewModel
    {
        $query = "SELECT * FROM products WHERE id=?";
        $query = $this->pdo->prepare($query);
        $query->execute([$id]);
        $queryResult = $query->fetchAll();
        if (empty($queryResult)) {
            die('Товар не найден');
        }
        $product = $queryResult[0];
        return new ProductEditorViewModel(
            $product['category_id'],
            $product['name'],
            $product['price'],
            $product['discount_percent'],
            $product['mechanism_type'],
            $product['country_name'],
            $product['available_count']
        );
    }

    /**
     * @return ProductViewModel[]
     */
    public function getProductsList(ProductFiltersBindingModel $model): array
    {
        $result = [];
        $query = 'SELECT * FROM products WHERE category_id=? AND (? IS NULL OR country_eng_name=?) ' .
            'AND (? IS NULL OR price >= ?) AND (? IS NULL OR price <= ?)';
        $query = $this->pdo->prepare($query);
        $query->execute([
            $model->categoryId,
            $model->countryEngName,
            $model->countryEngName,
            $model->minPrice,
            $model->minPrice,
            $model->maxPrice,
            $model->maxPrice
        ]);
        $products = $query->fetchAll();
        $mechanismTypes = explode(', ', $model->mechanismType);
        $anyMechTypes = ($mechanismTypes[0] !== "");
        $products = array_filter(
            $products,
            fn($pr) => $anyMechTypes == false || in_array($pr['mechanism_type'], $mechanismTypes)
        );
        if (!is_null($model->sortingType)) {
            $this->sortProductsList($products, $model->sortingType);
        }
        $totalPages = ceil(count($products) / CatalogConfig::PRODUCTS_PER_PAGE);
        if ($model->pageNumber > $totalPages) {
            $model->pageNumber = $totalPages;
        }
        $products = array_slice(
            $products,
            ($model->pageNumber - 1) * CatalogConfig::PRODUCTS_PER_PAGE,
            CatalogConfig::PRODUCTS_PER_PAGE
        );
        for ($i = 0; $i < count($products); $i++) {
            $product = $products[$i];
            $finalPrice = round($product['price'] * (1 - $product['discount_percent'] / 100));
            $id = $product['id'];
            $soldCount = $this->pdo->query(
                "SELECT sum(count) FROM order_products WHERE product_id=$id"
            )->fetchAll()[0][0];
            $soldCount ??= 0;
            $wasAddedToCart = false;
            if (!is_null($model->userPhone)) {
                $cartQuery = "SELECT count(*) FROM user_products WHERE product_id=$id " .
                    "AND user_phone=?";
                $cartQuery = $this->pdo->prepare($cartQuery);
                $cartQuery->execute([$model->userPhone]);
                $countInCart = $cartQuery->fetchAll()[0][0];
                $wasAddedToCart = ($countInCart > 0);
            }
            $result[] = new ProductViewModel(
                $product['id'],
                $product['photo_path'],
                $product['name'],
                $product['price'],
                $product['discount_percent'],
                number_format($finalPrice, 0, ' ', ' '),
                $product['mechanism_type'],
                $product['country_name'],
                $product['available_count'],
                $soldCount,
                $wasAddedToCart,
                $totalPages
            );
        }
        return $result;
    }

    /**
     * @return ProductViewModel[]
     */
    public function getSearchResult(string $phrase, string $categoryId, ?string $userPhone): array
    {
        $phrase = trim($phrase);
        $searchResult = $this->searchByExactEntry($phrase, $categoryId);
        if (empty($searchResult)) {
            $products = $this->pdo->query("SELECT * FROM products")->fetchAll();
            $phraseWords = explode(' ', $phrase);
            $this->searchByWords($products, $phraseWords, $searchResult);
            $this->fuzzySearchByWords($products, $phraseWords, $searchResult);
        }
        $result = [];
        foreach ($searchResult as $product) {
            $finalPrice = round((1 - $product['discount_percent'] / 100) * $product['price']);
            $id = $product['id'];
            $wasAddedToCart = false;
            if (!is_null($userPhone)) {
                $cartQuery = "SELECT count(*) FROM user_products WHERE product_id=$id " .
                    "AND user_phone=?";
                $cartQuery = $this->pdo->prepare($cartQuery);
                $cartQuery->execute([$userPhone]);
                $countInCart = $cartQuery->fetchAll()[0][0];
                $wasAddedToCart = ($countInCart > 0);
            }
            $soldCount = $this->pdo->query(
                "SELECT sum(count) FROM order_products WHERE product_id=$id"
            )->fetchAll()[0][0];
            $soldCount ??= 0;
            $result[] = new ProductViewModel(
                $product['id'],
                $product['photo_path'],
                $product['name'],
                $product['price'],
                $product['discount_percent'],
                number_format($finalPrice, 0, ' ', ' '),
                $product['mechanism_type'],
                $product['country_name'],
                $product['available_count'],
                $soldCount,
                $wasAddedToCart,
                0
            );
        }
        return $result;
    }

    public function createProduct(ProductBindingModel $model): void
    {
        $countryEngName = TranslationService::getEngTransliteration($model->countryName);
        $query = "
            INSERT INTO products(name, photo_path, price, discount_percent, 
            mechanism_type, country_name, country_eng_name, available_count, category_id) 
            VALUES (?,?,?,?,?,?,?,?,?)
        ";
        $query = $this->pdo->prepare($query);
        try {
            $this->pdo->beginTransaction();
            $query->execute([
                $model->name,
                $model->photoPath,
                $model->price,
                $model->discountPercent,
                $model->mechanismType,
                $model->countryName,
                $countryEngName,
                $model->availableCount,
                $model->categoryId
            ]);
            $this->pdo->commit();
        } catch (Exception $ex) {
            $this->pdo->rollBack();
        }

    }

    public function updateProduct(ProductBindingModel $model): void
    {
        $countryEngName = TranslationService::getEngTransliteration($model->countryName);
        $query = '';
        $params = [
            $model->categoryId,
            $model->availableCount,
            $model->name,
            $countryEngName,
            $model->countryName,
            $model->price,
            $model->discountPercent,
            $model->mechanismType,
            $model->id
        ];
        if (!is_null($model->photoPath)) {
            array_unshift($params, $model->photoPath);
            $query = "UPDATE products SET photo_path=?, category_id=?, available_count=?, name=?, country_eng_name=?,
                country_name=?, price=?, discount_percent=?, mechanism_type=? WHERE id=?";
        } else {
            $query = "UPDATE products SET category_id=?, available_count=?, name=?, country_eng_name=?,
                country_name=?, price=?, discount_percent=?, mechanism_type=? WHERE id=?";
        }
        $query = $this->pdo->prepare($query);
        try {
            $this->pdo->beginTransaction();
            $query->execute($params);
            $this->pdo->commit();
        } catch (Exception $ex) {
            $this->pdo->rollBack();
        }
    }

    public function deleteProduct(int $id): void
    {
        try {
            $this->pdo->beginTransaction();
            // Delete all sold product records with this product
            $orderProductsQuery = 'DELETE FROM order_products WHERE product_id=?';
            $orderProductsQuery = $this->pdo->prepare($orderProductsQuery);
            $orderProductsQuery->execute([$id]);
            // Delete this product from all users' carts
            $userProductsQuery = 'DELETE FROM user_products WHERE product_id=?';
            $userProductsQuery = $this->pdo->prepare($userProductsQuery);
            $userProductsQuery->execute([$id]);
            // Delete photo
            $photoPathQuery = "SELECT photo_path FROM products WHERE id=?";
            $photoPathQuery = $this->pdo->prepare($photoPathQuery);
            $photoPathQuery->execute([$id]);
            $path = $photoPathQuery->fetchAll()[0][0];
            $realPath = __DIR__ . '/../../' . $path;
            unlink($realPath);
            // Delete this product
            $query = "DELETE FROM products where id=?";
            $query = $this->pdo->prepare($query);
            $query->execute([$id]);
            $this->pdo->commit();
        } catch (Exception $ex) {
            $this->pdo->rollBack();
        }
    }

    /**
     * @param string $sortingType (BusinessLogic\Enums\SortingType constant)
     */
    private function sortProductsList(array &$products, string $sortingType): void
    {
        switch ($sortingType) {
            case SortingType::NAME:
                usort(
                    $products,
                    // sort ascending
                    fn($x, $y) => strcmp($x['name'], $y['name'])
                );
                break;
            case SortingType::DISCOUNT:
                usort(
                    $products,
                    // sort descending
                    fn($x, $y) => $x['discount_percent'] < $y['discount_percent']
                );
                break;
            case SortingType::PRICE_ASC:
            case SortingType::PRICE_DESC:
                usort(
                    $products,
                    function($x, $y) use ($sortingType) {
                        return ($sortingType == SortingType::PRICE_ASC) ?
                            $x['price'] > $y['price'] :
                            $x['price'] < $y['price'];
                    }
                );
                break;
            case SortingType::POPULARITY:
                $popularityValues = $this->pdo
                    ->query('SELECT product_id as id, sum(count) as popularity FROM ' .
                        ' order_products group by product_id order by product_id;')
                    ->fetchAll();
                if (empty($popularityValues)) {
                    return;
                }
                // Repack values from 2D into 1D array as product_id => popularity_value for convenience
                $popularityTable = [];
                foreach ($popularityValues as $row) {
                    $popularityTable[$row['id']] = $row['popularity'];
                }
                usort(
                    $products,
                    function($x, $y) use ($popularityTable) {
                        $xPop = 0;
                        $yPop = 0;
                        // if $x was ordered at least one time
                        if (isset($popularityTable[$x['id']])) {
                            $xPop = $popularityTable[$x['id']];
                        }
                        if (isset($popularityTable[$y['id']])) {
                            $yPop = $popularityTable[$y['id']];
                        }
                        // sort descending
                        return $xPop < $yPop;
                    }
                );
                break;
        }
    }

    private function searchByExactEntry(string $phrase, string $categoryId): array
    {
        $query = "SELECT * FROM products WHERE (?='all' OR category_id = ?) AND name LIKE ?;";
        $query = $this->pdo->prepare($query);
        $query->execute([$categoryId, $categoryId, "%$phrase%"]);
        return $query->fetchAll();
    }

    /**
     * Appends all the products, that contain at least one word from $words, into $result array
     * @param string[] $words
     */
    private function searchByWords(array $products, array $words, array &$result): void
    {
        foreach ($words as $word) {
            $curResult = array_filter(
                $products,
                fn($prod) => strpos($prod['name'], $word) !== false
            );
            foreach ($curResult as $curProduct) {
                $result[] = $curProduct;
            }
        }
    }

    /**
     * Appends all the products, that's fuzzy contain at least one word from $words, into $result array
     * @param string[] $words words to search
     * @param int $maxDist max Levenstein's distance between product's name word and word to search
     */
    private function fuzzySearchByWords(
        array $products,
        array $words,
        array &$result,
        int $maxDist = 3): void
    {
        foreach ($words as $phraseWord) {
            foreach ($products as $product) {
                $prodNameWords = explode(' ', $product['name']);
                foreach ($prodNameWords as $prodWord) {
                    if (levenshtein($prodWord, $phraseWord) <= $maxDist) {
                        $result[] = $product;
                    }
                }
            }
        }
    }
}
