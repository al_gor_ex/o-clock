<?php


namespace BusinessLogic\Services;


use BusinessLogic\Models\View\OrderCreationResult;
use BusinessLogic\Models\View\OrderProductViewModel;
use BusinessLogic\Models\View\OrdersListViewModel;
use BusinessLogic\Models\View\OrderViewModel;
use Carbon\Carbon;
use Configs\DatabaseConfig;
use Configs\OrdersListConfig;
use PDO;
use PDOException;

class OrderService
{
    private PDO $pdo;

    public function __construct()
    {
        $this->pdo = new PDO(
            DatabaseConfig::$dsn,
            DatabaseConfig::$userName,
            DatabaseConfig::$password
        );
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function createOrder(string $userPhone): OrderCreationResult
    {
        try {
            $cartService = new CartService();
            $this->pdo->beginTransaction();
            $unavailableNames = $cartService->getUnavailableProductsNames($userPhone);
            if (!empty($unavailableNames)) {
                return new OrderCreationResult(false, $unavailableNames);
            }
            $createdAt = Carbon::now()->toDateTimeString();
            $sumQuery = "
            SELECT 
                sum(price * count * (1 - discount_percent / 100)) AS sum
            FROM
                user_products AS UP
            JOIN
                products AS P ON UP.product_id = P.id
            WHERE
                user_phone = ?;
            ";
            $sumQuery = $this->pdo->prepare($sumQuery);
            $sumQuery->execute([$userPhone]);
            $sum = round($sumQuery->fetchAll()[0]['sum']);
            $description = $cartService->getCartJsonDescription($userPhone);
            $insertQuery = "
            INSERT INTO orders(created_at, sum, json_description, user_phone) 
            VALUES (?, ?, ?, ?)
            ";
            $insertQuery = $this->pdo->prepare($insertQuery);
            $insertQuery->execute([$createdAt, $sum, $description, $userPhone]);
            $orderId = $this->pdo->lastInsertId();
            $this->updateOrderProductsTable($userPhone, $orderId);
            $this->subtractOrderedProducts($userPhone);
            $cartService->clearCart($userPhone);
            $this->pdo->commit();
        } catch (PDOException $ex) {
            $this->pdo->rollBack();
        }
        return new OrderCreationResult(true);
    }

    public function getOrdersList(int $pageNumber, string $userPhone = null): OrdersListViewModel
    {
        $result = [];
        $query = "
        SELECT * FROM orders 
        WHERE (? IS NULL OR user_phone=?) 
        ORDER BY created_at DESC
        ";
        $query = $this->pdo->prepare($query);
        $query->execute([$userPhone, $userPhone]);
        $orders = $query->fetchAll();
        $countPerPage = OrdersListConfig::ORDERS_PER_PAGE;
        $totalPages = ceil(count($orders) / $countPerPage);
        if ($pageNumber > $totalPages) {
            $pageNumber = $totalPages;
        }
        $orders = array_slice(
            $orders,
            ($pageNumber - 1) * $countPerPage,
            $countPerPage
        );
        foreach ($orders as $order) {
            /**
             * @var OrderProductViewModel[]
             */
            $products = [];
            $description = json_decode($order['json_description'], true);
            $desProd = $description['products'];
            foreach ($desProd as $product) {
                $products[] = new OrderProductViewModel(
                    $product['name'],
                    $this->priceFmt($product['finalPrice']),
                    $product['count'],
                    $this->priceFmt($product['sum'])
                );
            }
            Carbon::setLocale('ru');
            $createdAt = Carbon::createFromTimeString($order['created_at'])
                ->isoFormat('D MMM Y H:mm');
            $result[] = new OrderViewModel(
                $order['id'],
                $createdAt,
                $this->priceFmt($order['sum']),
                $description['userPhone'],
                $description['userName'],
                $products
            );
        }
        return new OrdersListViewModel($result, $totalPages);
    }

    private function subtractOrderedProducts(string $userPhone): void
    {
        $productsQuery = "SELECT product_id as id, count FROM user_products where user_phone=?";
        $productsQuery = $this->pdo->prepare($productsQuery);
        $productsQuery->execute([$userPhone]);
        $products = $productsQuery->fetchAll();
        foreach ($products as $product) {
            $id = $product['id'];
            $soldCount = $product['count'];
            $query = "
            UPDATE products 
            SET available_count = available_count - $soldCount
            WHERE id = $id
            ";
            $this->pdo->exec($query);
        }
    }

    private function updateOrderProductsTable(string $userPhone, int $orderId): void
    {
        $query = "INSERT INTO order_products(product_id, order_id, count) VALUES (?, ?, ?)";
        $query = $this->pdo->prepare($query);
        $productsQuery = "SELECT product_id as id, count FROM user_products where user_phone=?";
        $productsQuery = $this->pdo->prepare($productsQuery);
        $productsQuery->execute([$userPhone]);
        $products = $productsQuery->fetchAll();
        foreach ($products as $product) {
            $query->execute([$product['id'], $orderId, $product['count']]);
        }
    }

    private function priceFmt($price): string
    {
        return number_format($price, 0, ',', ' ');
    }
}
