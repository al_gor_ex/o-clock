<?php


namespace BusinessLogic\Services;


use BusinessLogic\Enums\Role;
use BusinessLogic\Models\Binding\UserBindingModel;
use BusinessLogic\Models\View\AccountViewModel;
use Carbon\Carbon;
use Configs\AccessLifetimeConfig;
use Configs\AdminConfig;
use Configs\DatabaseConfig;
use Exception;
use PDO;

class UserService
{
    private PDO $pdo;

    public function __construct()
    {
        $this->pdo = new PDO(
            DatabaseConfig::$dsn,
            DatabaseConfig::$userName,
            DatabaseConfig::$password
        );
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function doesPhoneExist(string $phone): bool
    {
        $query = "SELECT phone FROM users WHERE phone=?";
        $query = $this->pdo->prepare($query);
        $query->execute([$phone]);
        $phones = $query->fetchAll();
        return (count($phones) > 0);
    }

    public function createUser(UserBindingModel $user): void
    {
        $createdAt = Carbon::now()->toDateTimeString();
        $query = "INSERT INTO users(phone, name, password, created_at) VALUES (?,?,?,?)";
        $query = $this->pdo->prepare($query);
        try {
            $this->pdo->beginTransaction();
            $query->execute([$user->phone, $user->name,
                password_hash($user->password, PASSWORD_DEFAULT), $createdAt]);
            $this->pdo->commit();
        } catch (Exception $ex) {
            $this->pdo->rollBack();
        }
    }

    public function login(string $phone, string $password): bool
    {
        $query = "SELECT * FROM users WHERE phone=?";
        $query = $this->pdo->prepare($query);
        $query->execute([$phone]);
        $result = $query->fetchAll();
        if (empty($result)) {
            return false;
        }
        $user = $result[0];
        if (password_verify($password, $user['password']) == false) {
            return false;
        }
        $token = $this->generateRandomString();
        $tokenExpiresAt = Carbon::now()
            ->addMinutes(AccessLifetimeConfig::$userTime)
            ->toDateTimeString();
        $updateQuery = "UPDATE users SET token=?, token_expires_at=? WHERE phone=?";
        $updateQuery = $this->pdo->prepare($updateQuery);
        $updateQuery->execute([$token, $tokenExpiresAt, $phone]);
        $lifetime = time() + AccessLifetimeConfig::$userTime * 60;
        setcookie('userName', $user['name'], $lifetime);
        setcookie('token', $token, $lifetime);
        setcookie('role', Role::USER, $lifetime);
        setcookie('phone', $phone);
        return true;
    }

    public function logout(string $token): void
    {
        $query = 'SELECT * FROM users WHERE token=?';
        $query = $this->pdo->prepare($query);
        $query->execute([$token]);
        $usersFound = $query->fetchAll();
        if (empty($usersFound)) {
            return;
        }
        $phoneToClear = $usersFound[0][0];
        $updateQuery = 'UPDATE users SET token=NULL, token_expires_at=NULL WHERE phone=?';
        $updateQuery = $this->pdo->prepare($updateQuery);
        $updateQuery->execute([$phoneToClear]);
    }

    public function loginAsAdmin(string $id, string $password): bool
    {
        if (($id == AdminConfig::$id && $password == AdminConfig::$password) == false) {
            return false;
        }
        $token = $this->generateRandomString();
        $tokenExpiresAt = Carbon::now()
            ->addMinutes(AccessLifetimeConfig::$adminTime)
            ->toDateTimeString();
        // Clear token records table
        $query = 'DELETE FROM admin_tokens';
        $this->pdo->exec($query);
        $insertQuery = 'INSERT INTO admin_tokens(token, expires_at) VALUES (?, ?)';
        $insertQuery = $this->pdo->prepare($insertQuery);
        $insertQuery->execute([$token, $tokenExpiresAt]);
        $lifetime = time() + AccessLifetimeConfig::$adminTime * 60;
        setcookie('token', $token, $lifetime);
        setcookie('role', Role::ADMIN, $lifetime);
        return true;
    }

    public function logoutAsAdmin(): void
    {
        $query = 'DELETE FROM admin_tokens';
        $this->pdo->exec($query);
    }

    public function verifyPassword(UserBindingModel $user): bool
    {
        $query = 'SELECT password FROM users WHERE phone=?';
        $query = $this->pdo->prepare($query);
        $query->execute([$user->phone]);
        $result = $query->fetchAll();
        if (empty($result)) {
            return false;
        }
        return (password_verify($user->password, $result[0][0]));
    }

    public function verifyToken(string $phone, string $token): bool
    {
        $query = 'SELECT token_expires_at FROM users WHERE phone=? AND token=?';
        $query = $this->pdo->prepare($query);
        $query->execute([$phone, $token]);
        $result = $query->fetchAll();
        if (empty($result)) {
            return false;
        }
        $expiresAt = $result[0][0];
        $crb = Carbon::createFromTimeString($expiresAt);
        if ($crb < Carbon::now()) {
            return false;
        }
        return true;
    }

    /**
     * @return AccountViewModel[]
     */
    public function getAccountsList(): array
    {
        $query = "
            SELECT 
                U.phone AS phone,
                U.name AS name,
                U.created_at AS registered_at,
                COUNT(O.id) AS orders_count,
                AVG(sum) AS average_sum,
                MAX(O.created_at) AS last_ordered_at
            FROM
                orders AS O
                    RIGHT OUTER JOIN
                users AS U ON O.user_phone = U.phone
            GROUP BY U.phone;
        ";
        $accounts = $this->pdo->query($query)->fetchAll();
        $result = [];
        Carbon::setLocale('ru');
        foreach ($accounts as $account) {
            $registrationDate = Carbon::createFromTimeString($account['registered_at'])
                ->isoFormat('D MMM Y');
            $lastOrderDate = null;
            if (!is_null($account['last_ordered_at'])) {
                $lastOrderDate = Carbon::createFromTimeString($account['last_ordered_at'])
                    ->isoFormat('D MMM Y');
                $lastOrderDate .= ' (' . Carbon::createFromTimeString($account['last_ordered_at'])
                    ->longRelativeToNowDiffForHumans() . ')';
            }
            $result[] = new AccountViewModel(
                $account['phone'],
                $account['name'],
                $registrationDate,
                (int)$account['orders_count'],
                $this->priceFmt($account['average_sum']),
                $lastOrderDate
            );
        }
        return $result;
    }

    public function updateUser(UserBindingModel $user): void
    {
        $query = '';
        if (empty($user->password)) {
            $query = 'UPDATE users SET name=? WHERE phone=?';
        } else {
            $query = 'UPDATE users SET name=?, password=? WHERE phone=?';
        }
        $query = $this->pdo->prepare($query);
        try {
            $this->pdo->beginTransaction();
            if (empty($user->password)) {
                $query->execute([$user->name, $user->phone]);
            } else {
                $query->execute([
                    $user->name,
                    password_hash($user->password, PASSWORD_DEFAULT),
                    $user->phone
                ]);
            }
            $this->pdo->commit();
        } catch (Exception $ex) {
            $this->pdo->rollBack();
        }
        setcookie('userName', $user->name);
    }

    public function deleteUser(string $phone): void
    {
        try {
            $cartService = new CartService();
            $this->pdo->beginTransaction();
            $cartService->clearCart($phone);
            $query = "DELETE FROM users WHERE phone=?";
            $query = $this->pdo->prepare($query);
            $query->execute([$phone]);
            $this->pdo->commit();
        } catch (Exception $ex) {
            $this->pdo->rollBack();
        }
    }

    private function generateRandomString($length = 32): string
    {
        return substr(sha1(rand()), 0, $length);
    }

    private function priceFmt($price): string
    {
        return number_format($price, 0, ',', ' ');
    }
}
