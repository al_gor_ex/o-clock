<?php


namespace BusinessLogic\Services;


use BusinessLogic\Models\Binding\CategoryBindingModel;
use BusinessLogic\Models\View\CategoryEditorViewModel;
use BusinessLogic\Models\View\CategoryViewModel;
use Configs\DatabaseConfig;
use Exception;
use PDO;

class CategoryService
{
    private PDO $pdo;

    public function __construct()
    {
        $this->pdo = new PDO(
            DatabaseConfig::$dsn,
            DatabaseConfig::$userName,
            DatabaseConfig::$password
        );
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * @return CategoryViewModel[]
     */
    public function getCategoriesList(): array
    {
        $result = [];
        $query = "SELECT * FROM categories";
        $categories = $this->pdo->query($query)->fetchAll();
        foreach ($categories as $category) {
            $result[] = new CategoryViewModel(
                $category['id'],
                $category['name'],
                $category['photo_path']
            );
        }
        return $result;
    }

    public function getCategoryForEditing(int $id): CategoryEditorViewModel
    {
        $query = "SELECT * FROM categories WHERE id=?";
        $query = $this->pdo->prepare($query);
        $query->execute([$id]);
        $queryResult = $query->fetchAll();
        if (empty($queryResult)) {
            die('Категория не найдена');
        }
        $category = $queryResult[0];
        return new CategoryEditorViewModel($category['name']);
    }

    public function createCategory(CategoryBindingModel $model): void
    {
        $query = "INSERT INTO categories(photo_path, name) VALUES (?, ?)";
        $query = $this->pdo->prepare($query);
        $query->execute([$model->photoPath, $model->name]);
    }

    public function updateCategory(CategoryBindingModel $model): void
    {
        $query = '';
        $params = [$model->name, $model->id];
        if (!is_null($model->photoPath)) {
            array_unshift($params, $model->photoPath);
            $query = "UPDATE categories SET photo_path=?, name=? WHERE id=?";
        } else {
            $query = "UPDATE categories SET name=? WHERE id=?";
        }
        $query = $this->pdo->prepare($query);
        $query->execute($params);
    }

    public function deleteCategory(int $id): void
    {
        // Delete all products in this category
        $productsQuery = "SELECT id FROM products WHERE category_id=?";
        $productsQuery = $this->pdo->prepare($productsQuery);
        $productsQuery->execute([$id]);
        $productIds = $productsQuery->fetchAll();
        if (!empty($productIds)) {
            $productService = new ProductService();
            foreach ($productIds as $idRecord) {
                $productService->deleteProduct($idRecord['id']);
            }
        }
        // Delete this category photo
        $photoPathQuery = "SELECT photo_path FROM categories WHERE id=?";
        $photoPathQuery = $this->pdo->prepare($photoPathQuery);
        $photoPathQuery->execute([$id]);
        $path = $photoPathQuery->fetchAll()[0][0];
        $realPath = __DIR__ . '/../../' . $path;
        unlink($realPath);
        // Delete this category record
        $query = "DELETE FROM categories where id=?";
        $query = $this->pdo->prepare($query);
        try {
            $this->pdo->beginTransaction();
            $query->execute([$id]);
            $this->pdo->commit();
        } catch (Exception $ex) {
            $this->pdo->rollBack();
        }

    }
}
