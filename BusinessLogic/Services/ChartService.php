<?php


namespace BusinessLogic\Services;


use Amenadiel\JpGraph\Graph\Graph;
use Amenadiel\JpGraph\Graph\PieGraph;
use Amenadiel\JpGraph\Graph\RadarGraph;
use Amenadiel\JpGraph\Plot\BarPlot;
use Amenadiel\JpGraph\Plot\PiePlot3D;
use Amenadiel\JpGraph\Plot\RadarPlot;
use Configs\DatabaseConfig;
use Exception;
use PDO;

class ChartService
{
    private PDO $pdo;

    public function __construct()
    {
        $this->pdo = new PDO(
            DatabaseConfig::$dsn,
            DatabaseConfig::$userName,
            DatabaseConfig::$password
        );
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * @return string Name of rendered image, which is placed in /storage/reports/charts folder
     * @throws Exception
     */
    public function makeCountriesRadarGraph(string $dateFrom, string $dateTo, int $maxAxisCount = 8): string
    {
        $query = "
        SELECT 
            P.country_name AS name, SUM(OP.count) AS count
        FROM
            orders AS O
                JOIN
            order_products AS OP ON O.id = OP.order_id
                JOIN
            products AS P ON OP.product_id = P.id
        WHERE
            O.created_at >= ?
                AND O.created_at <= ?
        GROUP BY P.country_name
        ORDER BY count DESC
        LIMIT $maxAxisCount
        ";
        $query = $this->pdo->prepare($query);
        $query->execute([$dateFrom, $dateTo]);
        $data = $query->fetchAll();
        if (empty($data)) {
            throw new Exception('No data found for radar graph');
        }
        // This makes the graph easier to understand
        shuffle($data);
        $titles = array_column($data, 'name');
        $data = array_column($data, 'count');
        $graph = new RadarGraph(640, 480);
        $graph->title->Set('Кол-во продаж по странам-производителям');
        $graph->title->SetFont(FF_VERDANA, FS_NORMAL, 12);
        $graph->SetTitles($titles);
        $graph->SetCenter(0.5, 0.55);
        $graph->SetColor('#d6faff');
        $graph->axis->SetColor('darkgray');
        $graph->grid->SetColor('darkgray');
        $graph->axis->title->SetFont(FF_ARIAL, FS_NORMAL, 12);
        $graph->axis->title->SetMargin(5);
        $graph->SetGridDepth(DEPTH_BACK);
        $graph->SetSize(0.6);
        $graph->grid->Show();
        $plot = new RadarPlot($data);
        $plot->SetColor('red@0.2');
        $plot->SetFillColor('red@0.7');
        $plot->mark->SetType(MARK_IMG_SBALL, 'red');
        $graph->Add($plot);
        $graph->img->SetImgFormat('png');
        $fileName = 'radar' . random_int(100_000, 1_000_000) . '.png';
        $filePath = __DIR__ . "/../../storage/reports/charts/$fileName";
        $graph->Stroke($filePath);
        return $fileName;
    }

    /**
     * @param int $maxPiecesCount If number of categories is bigger, others will be grouped into single piece
     * @return string Name of rendered image, which is placed in /storage/reports/charts folder
     * @throws Exception
     */
    public function makeCategoriesPieChart(string $dateFrom, string $dateTo, int $maxPiecesCount = 6): string
    {
        $query = "
        SELECT 
            C.name AS name, SUM(OP.count) AS count
        FROM
            orders AS O
                JOIN
            order_products AS OP ON O.id = OP.order_id
                JOIN
            products AS P ON OP.product_id = P.id
                JOIN
            categories AS C ON P.category_id = C.id
        WHERE
            O.created_at >= ?
                AND O.created_at <= ?
        GROUP BY C.id
        ORDER BY count DESC
        ";
        $query = $this->pdo->prepare($query);
        $query->execute([$dateFrom, $dateTo]);
        $data = $query->fetchAll();
        if (empty($data)) {
            throw new Exception('No data found for pie chart');
        }
        $totalOrdersCount = array_reduce($data, fn($sum, $ctg) => $sum + $ctg['count'], 0);
        // If count of categories is too big
        if (count($data) > $maxPiecesCount) {
            $data = array_slice($data, 0, $maxPiecesCount);
            // Count of orders in the categories which were not removed from the chart
            $shownOrdersCount = array_reduce($data, fn($sum, $ctg) => $sum + $ctg['count'], 0);
            $otherOrdersCount = $totalOrdersCount - $shownOrdersCount;
            $data[] = ['name' => 'Другие', 'count' => $otherOrdersCount];
        }
        $legends = array_column($data, 'name');
        $values = array_column($data, 'count');
        $graph = new PieGraph(640, 480);
        $graph->title->Set('Кол-во продаж по категориям');
        $graph->title->SetFont(FF_VERDANA, FS_BOLD);
        $plot = new PiePlot3D($values);
        $plot->SetLegends($legends);
        $plot->SetCenter(0.5, 0.4);
        $graph->Add($plot);
        $graph->img->SetImgFormat('png');
        $fileName = 'pie' . random_int(100_000, 1_000_000) . '.png';
        $filePath = __DIR__ . "/../../storage/reports/charts/$fileName";
        $graph->Stroke($filePath);
        return $fileName;
    }

    /**
     * Clusters all products into $rangesCount groups (each group has equal prices range length),
     * counts orders number for each group between specified dates, and builds a histogram
     * @return string Name of rendered image, which is placed in /storage/reports/charts folder
     */
    public function makePriceRangesHistogram(string $dateFrom, string $dateTo, int $rangesCount = 5): string
    {
        $bounds = $this->pdo->query("
        SELECT 
            MIN(price * (1 - discount_percent / 100)) AS min,
            MAX(price * (1 - discount_percent / 100)) AS max
        FROM
            products;
        ")->fetchAll();
        $minPrice = round($bounds[0]['min']);
        $maxPrice = round($bounds[0]['max']);
        $rangeLength = round(($maxPrice - $minPrice) / $rangesCount);
        $ranges = [];
        for ($i = 0; $i < $rangesCount + 1; $i++) {
            $ranges[] = $minPrice + $i * $rangeLength;
        }
        $query = "
        SELECT 
            P.price * (1 - P.discount_percent / 100) as price, 
            OP.count as count
        FROM
            orders AS O
                JOIN
            order_products AS OP ON O.id = OP.order_id
                RIGHT OUTER JOIN
            products AS P ON OP.product_id = P.id
        WHERE
            O.created_at >= ?
                AND O.created_at <= ?;
        ";
        $query = $this->pdo->prepare($query);
        $query->execute([$dateFrom, $dateTo]);
        $orders = $query->fetchAll();
        $legends = [];
        $values = [];
        for ($i = 0; $i < $rangesCount; $i++) {
            $lowerPrice = $ranges[$i];
            $upperPrice = $ranges[$i + 1];
            $matches = array_filter(
                $orders,
                fn($x) => $x['price'] >= $lowerPrice &&
                    $x['price'] <= $upperPrice
            );
            $matchedCount = array_reduce($matches, fn($sum, $prod) => $sum + $prod['count']);
            $legends[] = "$lowerPrice — $upperPrice";
            $values[] = $matchedCount ?? 0;
        }
        $graph = new Graph(640, 480);
        $graph->SetScale('textlin');
        $graph->xaxis->SetTickLabels($legends);
        $graph->xaxis->SetFont(FF_VERDANA, FS_NORMAL, 7);
        $graph->yaxis->SetLabelFormat('%0.0f');
        // Padding top
        $graph->yaxis->scale->SetAutoMax(max($values) + 2);
        $graph->title->Set('Кол-во продаж по интервалам цен');
        $graph->title->SetFont(FF_VERDANA, FS_BOLD);
        $graph->xaxis->title->Set('интервал');
        $graph->yaxis->title->Set('кол-во проданных товаров');
        $graph->yaxis->title->SetFont(FF_VERDANA, FS_BOLD);
        $graph->xaxis->title->SetFont(FF_VERDANA, FS_BOLD);
        $graph->SetMargin(50,30,20,60);
        $bplot = new BarPlot($values);
        $bplot->value->Show();
        $bplot->value->SetFormat('%0.0f');
        $bplot->SetFillColor('yellow');
        $graph->Add($bplot);
        $graph->img->SetImgFormat('png');
        $fileName = 'histogram' . random_int(100_000, 1_000_000) . '.png';
        $filePath = __DIR__ . "/../../storage/reports/charts/$fileName";
        $graph->Stroke($filePath);
        return $fileName;
    }
}
