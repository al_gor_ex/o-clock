<?php


namespace BusinessLogic\Services;


use Carbon\Carbon;
use Configs\DatabaseConfig;
use Exception;
use PDO;
use PhpOffice\PhpWord\Element\Section;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Shared\Converter;

class ReportService
{
    private PDO $pdo;

    public function __construct()
    {
        $this->pdo = new PDO(
            DatabaseConfig::$dsn,
            DatabaseConfig::$userName,
            DatabaseConfig::$password
        );
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * @return string Name of docx document, which is placed in /storage/reports folder
     */
    public function makeReport(string $dateFrom, string $dateTo): string
    {
        $this->deleteOldReportsAndCharts();
        Carbon::setLocale('ru');
        $dateFromCrb = Carbon::createFromIsoFormat('Y-MM-DD', $dateFrom);
        $dateToCrb = Carbon::createFromIsoFormat('Y-MM-DD', $dateTo);
        $query = "SELECT * FROM orders WHERE created_at >= ? AND created_at <= ?;";
        $query = $this->pdo->prepare($query);
        $query->execute([
            $dateFromCrb->startOfDay()->toDateTimeString(),
            $dateToCrb->endOfDay()->toDateTimeString()
        ]);
        $orders = $query->fetchAll();
        $doc = new PhpWord();
        $doc->getSettings()->setHideGrammaticalErrors(true);
        $doc->getSettings()->setHideSpellingErrors(true);
        $doc->setDefaultFontName('Times New Roman');
        $doc->setDefaultFontSize(14);
        $sectionStyle = [
            'orientation' => 'portrait',
            'marginTop' => Converter::cmToTwip(2),
            'marginLeft' => Converter::cmToTwip(3),
            'marginRight' => Converter::cmToTwip(1.5),
            'marginBottom' => Converter::cmToTwip(2),
            'colsNum' => 1,
            'pageNumberingStart' => 1
        ];
        $section = $doc->addSection($sectionStyle);
        $fontStyle = [
            'name' => 'Times New Roman',
            'size' => 14,
            'color' => '000000',
            'bold' => TRUE
        ];
        $paragraphStyle = [
            'align' => 'center',
            'spaceBefore' => 10
        ];
        $dateFromFmt = $dateFromCrb->isoFormat('D MMMM Y');
        $dateToFmt = $dateToCrb->isoFormat('D MMMM Y');
        $section->addText("Заказы за период с $dateFromFmt по $dateToFmt", $fontStyle, $paragraphStyle);

        $this->insertCharts(
            $section,
            $dateFromCrb->startOfDay()->toDateTimeString(),
            $dateToCrb->endOfDay()->toDateTimeString()
        );

        foreach ($orders as $order) {
            $fontStyle = [
                'name' => 'Times New Roman',
                'size' => 12,
                'color' => 'FF0000', // Red
                'bold' => true,
                'underline' => 'single'
            ];
            $paragraphStyle = [
                'align' => 'left',
                'spaceBefore' => 50
            ];
            $section->addText(" ", $fontStyle, $paragraphStyle);
            $section->addText("# {$order['id']}", $fontStyle, $paragraphStyle);
            $fontStyle = [
                'name' => 'Segoe UI',
                'size' => 12,
                'color' => '000000' // Black
            ];
            $paragraphStyle = [
                'align' => 'left',
                'spaceBefore' => 10
            ];
            $createdAt = Carbon::createFromTimeString($order['created_at'])
                ->isoFormat('D MMM Y H:mm');
            $section->addText($createdAt, $fontStyle, $paragraphStyle);
            $description = json_decode($order['json_description'], true);
            $userInfo = "{$description['userPhone']}\t\t{$description['userName']}";
            $section->addText($userInfo, $fontStyle, $paragraphStyle);
            $sum = $this->priceFmt($order['sum']);
            $fontStyle['bold'] = true;
            $section->addText($sum, $fontStyle, $paragraphStyle);
            unset($fontStyle['bold']);
            $products = $description['products'];
            $tableStyle = ['borderSize' => 1, 'borderColor' => '999999', 'cellMargin' => 100];
            $table = $section->addTable($tableStyle);
            $table->addRow();
            $table->addCell()->addText("№", $fontStyle);
            $table->addCell()->addText("Название", $fontStyle);
            $table->addCell()->addText("Цена (с учётом скидки)", $fontStyle);
            $table->addCell()->addText("Кол-во (шт.)", $fontStyle);
            $table->addCell()->addText("Стоимость", $fontStyle);

            $i = 1;
            foreach ($products as $product) {
                $table->addRow();
                $table->addCell()->addText($i, $fontStyle);
                $table->addCell()->addText($product['name'], $fontStyle);
                $table->addCell()->addText($this->priceFmt($product['finalPrice']), $fontStyle);
                $table->addCell()->addText($product['count'], $fontStyle);
                $table->addCell()->addText($this->priceFmt($product['sum']), $fontStyle);
                $i++;
            }

        }

        $fileName = 'report' . random_int(100_000, 1_000_000) . '.docx';
        $filePath = __DIR__ . "/../../storage/reports/$fileName";
        $writer = IOFactory::createWriter($doc, 'Word2007');
        $writer->save($filePath);
        return $fileName;
    }

    /**
     * @param int $lifeTime Files that were created more than $lifetime minutes ago, will be deleted
     */
    private function deleteOldReportsAndCharts(int $lifeTime = 1): void
    {
        $now = time();
        $lifeTime *= 60;
        $dirPaths = [
            __DIR__ . "/../../storage/reports/",
            __DIR__ . "/../../storage/reports/charts/"
        ];
        foreach ($dirPaths as $path) {
            $dd = opendir($path);
            while (($file = readdir($dd)) !== false) {
                if (is_dir($path . $file)) {
                    continue;
                }
                if ($now - filemtime($path . $file) >= $lifeTime) {
                    unlink($path . $file);
                }
            }
        }
    }

    private function insertCharts(Section $section, string $dateFrom, string $dateTo): void
    {
        $chartService = new ChartService();
        $chartsFolder = __DIR__ . '/../../storage/reports/charts/';
        $radarGraphName = null;
        $pieChartName = null;
        $histogramName = null;
        try {
            $radarGraphName = $chartService->makeCountriesRadarGraph($dateFrom, $dateTo);
            $pieChartName = $chartService->makeCategoriesPieChart($dateFrom, $dateTo);
            $histogramName = $chartService->makePriceRangesHistogram($dateFrom, $dateTo);
        } catch (Exception $ex) {}
        $chartNames = [$pieChartName, $histogramName, $radarGraphName];
        $imageStyle = ['width' => 400, 'height' => 300];
        foreach ($chartNames as $chartName) {
            if (!is_null($chartName)) {
                $section->addImage($chartsFolder . $chartName, $imageStyle);
            }
        }
    }

    private function priceFmt($price): string
    {
        return number_format($price, 0, ',', ' ') . " P";
    }
}
