<?php


namespace BusinessLogic\Services;


use BusinessLogic\Models\View\CartProductViewModel;
use BusinessLogic\Models\View\CartViewModel;
use Configs\CartConfig;
use Configs\DatabaseConfig;
use Exception;
use PDO;

class CartService
{
    private PDO $pdo;

    public function __construct()
    {
        $this->pdo = new PDO(
            DatabaseConfig::$dsn,
            DatabaseConfig::$userName,
            DatabaseConfig::$password
        );
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function addProduct(string $userPhone, int $productId): void
    {
        $query = 'INSERT INTO user_products(product_id, user_phone, count) VALUES (?, ?, 1)';
        $query = $this->pdo->prepare($query);
        try {
            $this->pdo->beginTransaction();
            $query->execute([$productId, $userPhone]);
            $this->pdo->commit();
        } catch (Exception $ex) {
            $this->pdo->rollBack();
        }
    }

    public function setProductCount(string $userPhone, int $productId, int $count): void
    {
        if ($count < 1 || $count > CartConfig::EACH_PRODUCT_MAX_COUNT) {
            $count = 1;
        }
        $query = 'UPDATE user_products SET count=? WHERE user_phone=? AND product_id=?';
        $query = $this->pdo->prepare($query);
        try {
            $this->pdo->beginTransaction();
            $query->execute([$count, $userPhone, $productId]);
            $this->pdo->commit();
        } catch (Exception $ex) {
            $this->pdo->rollBack();
        }
    }

    public function removeProduct(string $userPhone, int $productId): void
    {
        $query = 'DELETE FROM user_products WHERE user_phone=? AND product_id=?';
        $query = $this->pdo->prepare($query);
        try {
            $this->pdo->beginTransaction();
            $query->execute([$userPhone, $productId]);
            $this->pdo->commit();
        } catch (Exception $ex) {
            $this->pdo->rollBack();
        }
    }

    public function clearCart(string $userPhone): void
    {
        $query = 'DELETE FROM user_products WHERE user_phone=?';
        $query = $this->pdo->prepare($query);
        $query->execute([$userPhone]);
    }

    /**
     * @throws Exception
     */
    public function getProductsList(string $userPhone): CartViewModel
    {
        $countQuery = 'SELECT count(*) FROM user_products WHERE user_phone=?';
        $countQuery = $this->pdo->prepare($countQuery);
        $countQuery->execute([$userPhone]);
        $productsCount = $countQuery->fetchAll()[0][0];
        if ($productsCount == 0) {
            throw new Exception('Cart is empty');
        }
        $totalCostQuery = "
            SELECT 
            sum(price * count) AS price,
            sum(price * count * discount_percent / 100) AS discount_sum,
            sum(price * count * (1 - discount_percent / 100)) AS final_price
            FROM
                user_products AS UP
            JOIN
                products AS P
            ON UP.product_id = P.id
            WHERE
            UP.user_phone = ?;
        ";
        $totalCostQuery = $this->pdo->prepare($totalCostQuery);
        $totalCostQuery->execute([$userPhone]);
        $totalCostResult = $totalCostQuery->fetchAll()[0];
        $totalPrice = $totalCostResult['price'];
        $discountSum = $totalCostResult['discount_sum'];
        $totalFinalPrice = $totalCostResult['final_price'];
        $productsQuery = "
        SELECT 
               *,
               price * (1 - discount_percent / 100) AS final_price,
               price * count * (1 - discount_percent / 100) AS final_sum
        FROM
            user_products AS UP
        JOIN
            products AS P
        ON UP.product_id = P.id
        WHERE
            UP.user_phone = ?;
        ";
        $productsQuery = $this->pdo->prepare($productsQuery);
        $productsQuery->execute([$userPhone]);
        $dbProducts = $productsQuery->fetchAll();
        /** @var CartProductViewModel[]*/
        $products = [];
        foreach ($dbProducts as $dbProduct) {
            $products[] = new CartProductViewModel(
                $dbProduct['id'],
                $dbProduct['name'],
                $dbProduct['photo_path'],
                $this->priceFmt($dbProduct['price']),
                $dbProduct['discount_percent'],
                $this->priceFmt($dbProduct['final_price']),
                $dbProduct['count'],
                $this->priceFmt($dbProduct['final_sum'])
            );
        }
        $result = new CartViewModel(
            $productsCount,
            $this->priceFmt($totalPrice),
            $this->priceFmt($discountSum),
            $this->priceFmt($totalFinalPrice),
            $products
        );
        return $result;
    }

    /**
     * @return string[]
     */
    public function getUnavailableProductsNames(string $userPhone): array
    {
        $query = "
        SELECT 
            name
        FROM
            user_products AS UP
        JOIN
            products AS P 
        ON UP.product_id = P.id
        WHERE
            UP.user_phone = ?
            AND P.available_count < UP.count;
        ";
        $query = $this->pdo->prepare($query);
        $query->execute([$userPhone]);
        $result = $query->fetchAll();
        return (empty($result)) ? [] : array_column($result, 'name');
    }

    public function getCartJsonDescription(string $userPhone): string
    {
        $result = [
            'userPhone' => htmlspecialchars($_COOKIE['phone']),
            'userName' => htmlspecialchars($_COOKIE['userName']),
            'products' => []
        ];
        $query = "
        SELECT
	        name,
            price * (1 - discount_percent / 100) AS final_price,
	        count,
            price * count * (1 - discount_percent / 100) AS sum
        FROM
            user_products AS UP
        JOIN
            products AS P ON UP.product_id = P.id
        WHERE
            user_phone = ?;
        ";
        $query = $this->pdo->prepare($query);
        $query->execute([$userPhone]);
        $products = $query->fetchAll();
        foreach ($products as $product) {
            $result['products'][] = [
                'name' => $product['name'],
                'finalPrice' => round($product['final_price']),
                'count' => (int)$product['count'],
                'sum' => round($product['sum'])
            ];
        }
        return json_encode($result);
    }

    private function priceFmt($price): string
    {
        return number_format($price, 0, ',', ' ');
    }
}
