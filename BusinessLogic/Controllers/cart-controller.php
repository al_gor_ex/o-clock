<?php

use BusinessLogic\Enums\CartOperation;
use BusinessLogic\Services\CartService;
use BusinessLogic\Services\UserService;

require_once __DIR__ . '/../../vendor/autoload.php';

function verifyAuthToken(string $userPhone): void
{
    $authorizationHeader = apache_request_headers()['Authorization'];
    $token = explode(' ', $authorizationHeader)[1];
    $userService = new UserService();
    if ($userService->verifyToken($userPhone, $token) == false) {
        http_response_code(401);
        exit();
    }
}

$operation = null;
$userPhone = null;
$productId = null;

$requestBody = file_get_contents('php://input');
$bodyData = json_decode($requestBody, true);
if (isset($bodyData['operation'])) {
    $operation = $bodyData['operation'];
}
if (isset($bodyData['userPhone'])) {
    $userPhone = $bodyData['userPhone'];
}
if (isset($bodyData['productId'])) {
    $productId = $bodyData['productId'];
}
if (is_null($operation) || is_null($userPhone) || is_null($productId)) {
    http_response_code(400);
    exit();
}

verifyAuthToken($userPhone);
$cartService = new CartService();

switch ($operation) {
    case CartOperation::ADD:
        $cartService->addProduct($userPhone, $productId);
        http_response_code(200);
        break;
    case CartOperation::REMOVE:
        $cartService->removeProduct($userPhone, $productId);
        http_response_code(200);
        break;
}
