<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once 'navbar.php';

$smarty = new Smarty();
require_once 'navbar-assigns.php';
$smarty->assign('url', basename(__FILE__));
$smarty->display('home.tpl');
