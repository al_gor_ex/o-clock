<?php


namespace Configs;


class AccessLifetimeConfig
{
    // Tokens lifetime in minutes
    public static $userTime = 20;
    public static $adminTime = 10;
}
