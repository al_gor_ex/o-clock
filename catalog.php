<?php

use BusinessLogic\Models\Binding\ProductFiltersBindingModel;
use BusinessLogic\Services\ProductService;

require_once __DIR__ . '/vendor/autoload.php';
require_once 'navbar.php';

$categoryId = null;
$userPhone = null;
$sortingType = null;
$countryEngName = null;
$mechanismType = null;
$minPrice = null;
$maxPrice = null;
$page = 1;

if (isset($_GET['category-id'])) {
    $categoryId = htmlspecialchars($_GET['category-id']);
}
if (isset($_COOKIE['phone'])) {
    $userPhone = htmlspecialchars($_COOKIE['phone']);
}
if (isset($_COOKIE['sortBy'])) {
    $sortingType = htmlspecialchars($_COOKIE['sortBy']);
}
if (isset($_COOKIE['countryEngName'])) {
    $countryEngName = htmlspecialchars($_COOKIE['countryEngName']);
    if ($countryEngName == 'all') {
        $countryEngName = null;
    }
}
if (isset($_COOKIE['mechanismTypes'])) {
    $mechanismType = htmlspecialchars($_COOKIE['mechanismTypes']);
}
if (isset($_COOKIE['minPrice'])) {
    $minPrice = htmlspecialchars($_COOKIE['minPrice']);
}
if (isset($_COOKIE['maxPrice'])) {
    $maxPrice = htmlspecialchars($_COOKIE['maxPrice']);
}
if (isset($_GET['page'])) {
    $page = htmlspecialchars($_GET['page']);
}

if ($page < 1) {
    $page = 1;
}
$productService = new ProductService();
$priceRange = $productService->getPriceRange($categoryId);
$countriesList = $productService->getCountriesList($categoryId);


$productsList = $productService->getProductsList(
    new ProductFiltersBindingModel(
        $categoryId, $userPhone, $sortingType, $countryEngName,
        $mechanismType, $minPrice, $maxPrice, $page
    )
);
$pagesCount = $productsList[0]->totalPagesCount ?? 0;
if ($page > $pagesCount) {
    $page = $pagesCount;
}
// 3 items in a row
$productsList = array_chunk($productsList, 3);

$smarty = new Smarty();
require_once 'navbar-assigns.php';
$smarty->assign('url', basename(__FILE__));
$smarty->assign('priceRange', $priceRange);
$smarty->assign('countriesList', $countriesList);
$smarty->assign('productsList', $productsList);
$smarty->assign('pagesCount', $pagesCount);
$smarty->assign('page', $page);
$smarty->assign('categoryId', $categoryId);
// Filter values
$smarty->assign('sortingType', $sortingType);
$smarty->assign('countryEngName', $countryEngName);
$smarty->assign('mechanismType', $mechanismType);
$smarty->assign('minPrice', $minPrice);
$smarty->assign('maxPrice', $maxPrice);
$smarty->display('catalog.tpl');
