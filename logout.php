<?php

use BusinessLogic\Enums\Role;
use BusinessLogic\Guards\RoleGuard;
use BusinessLogic\Services\UserService;

require_once __DIR__ . '/vendor/autoload.php';

(new RoleGuard())->letIn(Role::USER_OR_ADMIN);

function redirect(): void
{
    header('Location: home.php');
    exit();
}

function unsetCookies(): void
{
    $cookieNames = ['role', 'token', 'userName', 'phone'];
    foreach ($cookieNames as $name) {
        setcookie($name, '', time() - 1);
    }
}

$token = null;
$role = null;

if (isset($_COOKIE['token'])) {
    $token = htmlspecialchars($_COOKIE['token']);
}
if (isset($_COOKIE['role'])) {
    $role = htmlspecialchars($_COOKIE['role']);
}

$userService = new UserService();
if ($role == Role::USER) {
    $userService->logout($token);
} else {
    $userService->logoutAsAdmin();
}
unsetCookies();
redirect();
