<?php

use Configs\YandexMapConfig;

require_once __DIR__ . '/vendor/autoload.php';
require_once 'navbar.php';

$apiKey = YandexMapConfig::$apiKey;

$smarty = new Smarty();
require_once 'navbar-assigns.php';
$smarty->assign('url', basename(__FILE__));
$smarty->assign('apiKey', $apiKey);
$smarty->display('contacts.tpl');
