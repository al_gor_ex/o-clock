<?php

use BusinessLogic\Enums\Role;
use BusinessLogic\Guards\RoleGuard;
use BusinessLogic\Models\Binding\CategoryBindingModel;
use BusinessLogic\Services\CategoryService;
use Intervention\Image\ImageManagerStatic;

require_once __DIR__ . '/vendor/autoload.php';

(new RoleGuard())->letIn(Role::ADMIN);

if (!isset($_REQUEST['id'])) {
    die('Неверные параметры запроса');
}
$id = $_REQUEST['id'];
$isNew = false;
if ($id == -1) {
    $id = null;
    $isNew = true;
}
$photoPath = null;
if (isset($_FILES['photo']) && $_FILES['photo']['name'] != '') {
    $uploadName = $_FILES['photo']['tmp_name'];
    $newName = 'storage/uploads/category/' . random_int(100_000, 1_000_000) . '.' .
        explode('.', $_FILES['photo']['name'])[1]; // file extension
    move_uploaded_file($uploadName, $newName);
    ImageManagerStatic::make($newName)->resize(500, 500)->save();
    $photoPath = $newName;
}
$name = htmlspecialchars($_POST['name']);
$categoryService = new CategoryService();
$model = new CategoryBindingModel($id, $photoPath, $name);
if ($isNew) {
    $categoryService->createCategory($model);
} else {
    $categoryService->updateCategory($model);
}
header("Location: categories.php");
