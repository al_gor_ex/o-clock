<?php

use BusinessLogic\Enums\Role;
use BusinessLogic\Guards\RoleGuard;
use BusinessLogic\Services\OrderService;

require_once __DIR__ . '/vendor/autoload.php';

(new RoleGuard())->letIn(Role::USER);

$userPhone = htmlspecialchars($_COOKIE['phone']);
$orderService = new OrderService();
$result = $orderService->createOrder($userPhone);

$smarty = new Smarty();
$smarty->assign('result', $result);
$smarty->display('checkout.tpl');
