<?php

use BusinessLogic\Enums\Role;
use BusinessLogic\Guards\RoleGuard;
use BusinessLogic\Services\CartService;

require_once __DIR__ . '/vendor/autoload.php';

(new RoleGuard())->letIn(Role::USER);

$userPhone = null;
$productId = null;
$count = null;
if (isset($_GET['phone'])) {
    $userPhone = htmlspecialchars($_GET['phone']);
}
if (isset($_GET['count'])) {
    $count = htmlspecialchars($_GET['count']);
}
if (isset($_GET['product'])) {
    $productId = htmlspecialchars($_GET['product']);
}
if (is_null($userPhone) || is_null($productId) || is_null($count)) {
    die('Неверные параметры запроса');
}

$cartService = new CartService();
$cartService->setProductCount($userPhone, $productId, $count);

$redirectUrl = 'cart.php#productCount' . $productId;
header("Location: $redirectUrl");
