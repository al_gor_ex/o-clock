<?php

use BusinessLogic\Enums\Role;
use BusinessLogic\Guards\RoleGuard;
use BusinessLogic\Models\Binding\UserBindingModel;
use BusinessLogic\Services\UserService;

require_once __DIR__ . '/vendor/autoload.php';

(new RoleGuard())->letIn(Role::ANON);

$phone = null;
$name = null;
$password = null;
if (isset($_POST['phone'])) {
    $phone = htmlspecialchars($_POST['phone']);
}
if (isset($_POST['name'])) {
    $name = htmlspecialchars($_POST['name']);
}
if (isset($_POST['password'])) {
    $password = htmlspecialchars($_POST['password']);
}
if (is_null($phone) || is_null($name) || is_null($password)) {
    die("Неверные параметры запроса");
}
$userService = new UserService();
if ($userService->doesPhoneExist($phone)) {
    header('Location: registration.php?phone-occupied=true');
    exit();
}
$userService->createUser(new UserBindingModel($phone, $name, $password));

$smarty = new Smarty();
$smarty->display('registration-processing.tpl');
