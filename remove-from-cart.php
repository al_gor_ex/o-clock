<?php

use BusinessLogic\Enums\Role;
use BusinessLogic\Guards\RoleGuard;
use BusinessLogic\Services\CartService;

require_once __DIR__ . '/vendor/autoload.php';

(new RoleGuard())->letIn(Role::USER);

$userPhone = null;
$productId = null;
$redirectUrl = null;
if (isset($_GET['phone'])) {
    $userPhone = htmlspecialchars($_GET['phone']);
}
if (isset($_GET['product'])) {
    $productId = htmlspecialchars($_GET['product']);
}
if (isset($_GET['redirect-id'])) {
    $redirectUrl = 'cart.php#' . htmlspecialchars($_GET['redirect-id']);
}
if (is_null($userPhone) || is_null($productId) || is_null($redirectUrl)) {
    die('Неверные параметры запроса');
}

$cartService = new CartService();
$cartService->removeProduct($userPhone, $productId);
header("Location: $redirectUrl");
