<?php

use BusinessLogic\Enums\Role;
use BusinessLogic\Guards\RoleGuard;
use BusinessLogic\Services\CartService;

require_once __DIR__ . '/vendor/autoload.php';
require_once 'navbar.php';

(new RoleGuard())->letIn(Role::USER);
$userPhone = htmlspecialchars($_COOKIE['phone']);

$cartService = new CartService();
$anyProducts = false;
$cartProductsList = null;
try {
    $cartProductsList = $cartService->getProductsList($userPhone);
    $anyProducts = true;
} catch (Exception $e) {}

$smarty = new Smarty();
require_once 'navbar-assigns.php';
$smarty->assign('url', basename(__FILE__));
$smarty->assign('anyProducts', $anyProducts);
$smarty->assign('productsList', $cartProductsList);
$smarty->assign('userPhone', $userPhone);
$smarty->display('cart.tpl');
