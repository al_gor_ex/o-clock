<?php

use BusinessLogic\Enums\Role;
use BusinessLogic\Guards\RoleGuard;
use BusinessLogic\Services\CategoryService;

require_once __DIR__ . '/vendor/autoload.php';

(new RoleGuard())->letIn(Role::ADMIN);

if (!isset($_GET['id'])) {
    die('Неверные параметры запроса');
}
$id = $_GET['id'];

$categoryService = new CategoryService();
$categoryService->deleteCategory($id);

header('Location: categories.php');
