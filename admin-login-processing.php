<?php

use BusinessLogic\Enums\Role;
use BusinessLogic\Guards\RoleGuard;
use BusinessLogic\Services\UserService;

require_once __DIR__ . '/vendor/autoload.php';

(new RoleGuard())->letIn(Role::ANON);

$id = null;
$password = null;

if (isset($_POST['id'])) {
    $id = htmlspecialchars($_POST['id']);
}
if (isset($_POST['password'])) {
    $password = htmlspecialchars($_POST['password']);
}
if (is_null($id) || is_null($password)) {
    die("Неверные параметры запроса");
}
$userService = new UserService();
$loginResult = $userService->loginAsAdmin($id, $password);

$smarty = new Smarty();
$smarty->assign('result', $loginResult);
$smarty->display('admin-login-processing.tpl');
