# Магазин часов "O'Clock"
Веб-приложение позволяет управлять товарами, категориями, пользователями и заказами в магазине часов

Также присутствует нечёткий поиск товаров, их фильтрация и сортировка

В панели администратора доступна генерация отчёта по продажам магазина

Вы можете найти инструкцию по установке в файле
```
docs/installation.txt
```

При создании приложения использовались следующие технологии:
- PHP 7.4
- SQL (MySQL dialect, PDO)
- Smarty
- HTML
- CSS
- Bootstrap
- JS + TS
- jQuery
- PhpUnit

ER-диаграмма базы данных:
![](https://sun9-41.userapi.com/impg/lf5I9Xe2PHx7P6isPE0fx5XlczFb6lQ0ZVKEMg/kVb2R0lBXGA.jpg?size=988x501&quality=96&sign=10b3f0aa0203d0ddccc4edaf299070ec&type=album)

## Скриншоты
Главная страница
![](https://sun9-48.userapi.com/impg/ER1_7yBvXZ7dsB98V_R5IohqnnAcQTRrvRzkNQ/qfGlbkD5yIk.jpg?size=1346x1193&quality=96&sign=26c76e4149101bb31760cd096f245b1b&type=album)

Контакты
![](https://sun9-63.userapi.com/impg/GCuQkQ49QmatPGsvzw_E6Tnlo61T6RudSt-neQ/vCvyX-gKG8Q.jpg?size=1343x1265&quality=96&sign=355a21c2e42fbfbcd4dd6738dc594013&type=album)

Категории
![](https://sun9-61.userapi.com/impg/M--lyJMDTfGoTztizmzT8BrOqs9NY40B3SlCpQ/2NKR-b749h4.jpg?size=1345x1067&quality=96&sign=0c3f8a5219c2d2f2b8feb6ef9237d045&type=album)

Товары
![](https://sun9-76.userapi.com/impg/1JkpWaTeqhnkEwVIMq2UPHFPbxJxBxUwI3y3MQ/ttEsZbyf1cU.jpg?size=1366x768&quality=96&sign=bd9e47643d53edd9507cbb349f011acb&type=album)
<br>
![](https://sun9-29.userapi.com/impg/O4OEPke_-SUoWMfWWCasJGVQvU2NnY1VLa1Fcg/QmBcmHMS7w0.jpg?size=1366x768&quality=96&sign=3784cf51a25e1fed56432b944943f2d2&type=album)

Регистрация покупателя
![](https://sun9-12.userapi.com/impg/nXmjW4aNsiyXQuXqNZXTZLmYAg5sr34zRoOCRQ/xeO5-k9ZeHQ.jpg?size=1366x768&quality=96&sign=8c4ec4359c6d889394ea598d1e882715&type=album)

Вход покупателя
![](https://sun9-84.userapi.com/impg/TB-Ci61syZ62Iv5XX6VVQJNr6N3vKU2_i0m00Q/hH9kkX9pgpI.jpg?size=1366x768&quality=96&sign=ff9d600e78d2d796f433ae21144719f7&type=album)

Изменение профиля покупателя
![](https://sun9-9.userapi.com/impg/wQRD0Z13SoQGcdtkiC6jG5CqTOyg8j-Ln62X6g/rEIY8VjLjoY.jpg?size=1366x768&quality=96&sign=7d18a0f7cfbc0465efa2f9dac623ec34&type=album)

Добавление товаров в корзину
![](https://sun9-86.userapi.com/impg/nlm9h1C6E1aFYA5lbCuPgjbI9QJF6QyVPaABdg/OL8iauK1Ifg.jpg?size=1366x768&quality=96&sign=34057ee91e59e37889fce9e3b3550336&type=album)

Поиск товаров
![](https://sun9-70.userapi.com/impg/DMGFDfXw0x2A-3uneKbLBfxLNrmbwS7ErgLweg/-FjEyKEvPGo.jpg?size=1366x768&quality=96&sign=7c8cc3831e74464f6424e37f6e703cbc&type=album)

Корзина
![](https://sun9-59.userapi.com/impg/F6WxViIpXQo1gZ20NBsOxl_ujBUhDxDKkjFYnA/IHjhubUo7BE.jpg?size=1366x768&quality=96&sign=b31d71305df1b2375ca9f3c9b85f333f&type=album)

Ошибка оформления заказа (недостаточно товаров)
![](https://sun9-84.userapi.com/impg/9JYmtxXegxXnA3omwVcR8BdCSWTnWIOmkoaftw/pVbSLSQNOKc.jpg?size=795x319&quality=96&sign=ee6baf282ebacb5e35c07ac59100d12c&type=album)

Успешное создание заказа
![](https://sun9-14.userapi.com/impg/VHEnelkpRc6vOoas4f4cm2loIvFeKEo-Fygcug/kMdlEKYSVA4.jpg?size=353x173&quality=96&sign=f87fc1861dcae71c18f7eabc51895708&type=album)

Заказы покупателей
![](https://sun9-4.userapi.com/impg/GLo2KykqUPC2d9ccDX_0gdcpvKWjTKkujvG8qg/Tw6661XUQKE.jpg?size=1366x768&quality=96&sign=35f6a68d0ef1c74595f9b8ca5cdc22ba&type=album)

Вход в аккаунт администратора
![](https://sun9-19.userapi.com/impg/USCRUhub3FYJVsj33FKTPdE_GvT82rhgA3SzPw/xw16Ip23vI4.jpg?size=468x337&quality=96&sign=9d08fa510a835958579424c73ef44f03&type=album)

Заказы всех покупателей в панели администратора
![](https://sun9-25.userapi.com/impg/klknxN88jNBFtYfxaViQ7mJyLOupeS8hMWSoxw/63fIudN4eE0.jpg?size=1366x768&quality=96&sign=df6f9ebe9eebc15ec1d6e778f60f9e5d&type=album)

Выбор временного промежутка для генерации отчёта
![](https://sun9-8.userapi.com/impg/iyg6651tOr1S3rzKD4MwO-HRL0vRbYiT-YzNdA/6IEz7WnKAWQ.jpg?size=1366x325&quality=96&sign=5a1f2f1fda4403dd53d3d8b8d78c5f34&type=album)

Содержимое отчёта
![](https://sun9-66.userapi.com/impg/M8loCQW3Uspzv0fRb2jTm25cr7xZgDuPFpg67A/cE1EZYtJ5xs.jpg?size=1150x628&quality=96&sign=dfdb8486a4a4ed7fbe39295b59c222d4&type=album)
![](https://sun9-59.userapi.com/impg/ov5XsIyryd0eqJ_Y76oFWJtq4JaveYWNqbQ8jg/fCq0MvGZ41I.jpg?size=1141x606&quality=96&sign=244c5ec00a3d8455a493fd16f9d56fe2&type=album)
![](https://sun9-26.userapi.com/impg/kO8lbRX2mqcGleE56Nn2y3J-ZbvYEnc8IG0PVw/oTTIDweVw_U.jpg?size=1146x603&quality=96&sign=6d81b38515c529152e054feb548f5ce2&type=album)
![](https://sun9-53.userapi.com/impg/dMKY9BuKs_fomlHDYVf5F5Y9sZAqIap16NTj0A/ieJKOse_vAs.jpg?size=1152x610&quality=96&sign=6644fa7bb8ffb13501ce7d3ee90d043a&type=album)

Управление аккаунтами покупателей
![](https://sun9-65.userapi.com/impg/5Yx-_Pnqa_MPDhiPAm38zqf_kdEpNKC3Mg9uEw/1ubsL5-gUfI.jpg?size=1366x359&quality=96&sign=a3e2a9a2aab3942918272eeee2ae073f&type=album)

Вид списка категорий для роли администратора
![](https://sun9-14.userapi.com/impg/ZrKrcTFA415_xpb-eFtqbaeRMVXQirGMQCuWmA/VBEKo1tyBFM.jpg?size=1366x768&quality=96&sign=4ce3e45e9b02342a5fd2a907a197b57d&type=album)

Редактирование категории
![](https://sun9-66.userapi.com/impg/Qt5e9ZB46fIIYOCMyW3PNHJi4PxtCjdZDv1APQ/0O-SEOj0PEA.jpg?size=373x293&quality=96&sign=fceeeaa72976b33a801494013c437051&type=album)

Редактирование товара
![](https://sun9-13.userapi.com/impg/-1UV45WlIpKXPHUAVMn63rE93r1fHtuthnK2HA/VV11k06a39Y.jpg?size=383x768&quality=96&sign=cde967375aae718186cbe31d99a6d772&type=album)