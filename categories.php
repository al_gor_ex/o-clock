<?php

use BusinessLogic\Services\CategoryService;

require_once __DIR__ . '/vendor/autoload.php';
require_once 'navbar.php';

$categoryService = new CategoryService();
$categories = $categoryService->getCategoriesList();
// 3 items in a row
$categories = array_chunk($categories, 3);

$smarty = new Smarty();
require_once 'navbar-assigns.php';
$smarty->assign('url', basename(__FILE__));
$smarty->assign('categories', $categories);
$smarty->display('categories.tpl');
