<?php

use BusinessLogic\Enums\Role;
use BusinessLogic\Guards\RoleGuard;
use BusinessLogic\Services\CategoryService;

require_once __DIR__ . '/vendor/autoload.php';

(new RoleGuard())->letIn(Role::ADMIN);

if (!isset($_GET['id'])) {
    die('Неверные параметры запроса');
}
$id = $_GET['id'];
$isNew = false;
if ($id == -1) {
    $isNew = true;
}
$category = null;
if ($isNew == false) {
    $categoryService = new CategoryService();
    $category = $categoryService->getCategoryForEditing($id);
}

$smarty = new Smarty();
$smarty->assign('id', $id);
$smarty->assign('isNew', $isNew);
$smarty->assign('category', $category);
$smarty->display('new-edit-category.tpl');
