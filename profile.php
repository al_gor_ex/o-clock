<?php

use BusinessLogic\Enums\Role;
use BusinessLogic\Guards\RoleGuard;

require_once __DIR__ . '/vendor/autoload.php';
require_once 'navbar.php';

(new RoleGuard())->letIn(Role::USER);
$phone = htmlspecialchars($_COOKIE['phone']);

$smarty = new Smarty();
require_once 'navbar-assigns.php';
$smarty->assign('url', basename(__FILE__));
$smarty->assign('phone', $phone);
$smarty->display('profile.tpl');
