<?php

use BusinessLogic\Enums\Role;

require_once __DIR__ . '/vendor/autoload.php';

$role = Role::ANON;
if (isset($_COOKIE['role'])) {
    $role = htmlspecialchars($_COOKIE['role']);
}
$userName = '';
if (isset($_COOKIE['userName'])) {
    $userName = htmlspecialchars($_COOKIE['userName']);
}
