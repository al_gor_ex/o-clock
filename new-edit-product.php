<?php

use BusinessLogic\Enums\Role;
use BusinessLogic\Guards\RoleGuard;
use BusinessLogic\Services\CategoryService;
use BusinessLogic\Services\ProductService;

require_once __DIR__ . '/vendor/autoload.php';

(new RoleGuard())->letIn(Role::ADMIN);

if (!isset($_GET['id'])) {
    die('Неверные параметры запроса');
}
$id = $_GET['id'];
$isNew = false;
if ($id == -1) {
    $isNew = true;
}
$product = null;
if (!$isNew) {
    $productService = new ProductService();
    $product = $productService->getProductForEditing($id);
}

$categoryService = new CategoryService();
$categoriesList = $categoryService->getCategoriesList();

$smarty = new Smarty();
$smarty->assign('id', $id);
$smarty->assign('isNew', $isNew);
$smarty->assign('product', $product);
$smarty->assign('categoriesList', $categoriesList);
$smarty->display('new-edit-product.tpl');
