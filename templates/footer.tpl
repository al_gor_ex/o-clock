<head>
    <link rel="stylesheet" href="assets/css/footer.css"/>
</head>
<body>
<div class="container-fluid footer-wrapper py-3 mt-5 px-5">
    <div class="h4 brand-name">
        O'Clock
    </div>
    <div class="h5 brand-description">
        Магазин часов от производителей со всего мира
    </div>
    <div class="d-flex flex-wrap justify-content-between align-items-center">
        <div class="h6 mt-2">
            E-mail администратора: admin@example.com
        </div>
        <div>
            <span class="badge badge-dark rounded-circle text-white social-link">
                VK
            </span>
            <span class="badge badge-light rounded-circle social-link mx-4">
                IN
            </span>
            <span class="badge badge-primary rounded-circle text-white social-link">
                FB
            </span>
        </div>
    </div>
    {if $role == BusinessLogic\Enums\Role::ANON}
    <a href="admin-login.html">
        Управление сайтом
    </a>
    {/if}
    <div class="text-center mt-3">
        &copy; O'Clock, 2021
    </div>
</div>
</body>
