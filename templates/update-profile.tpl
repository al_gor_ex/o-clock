<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Изменение профиля - O'Clock</title>
    <link rel="shortcut icon" type="image/png" href="assets/img/icons/favicon.ico">
    {if $result == true}
    {literal}
        <script>
            alert('Профиль успешно изменён');
            document.location.href = 'cart.php';
        </script>
    {/literal}
    {else}
    {literal}
        <script>
            alert('Неверные данные');
            document.location.href = 'profile.php';
        </script>
    {/literal}
    {/if}
</head>
<body>
</body>
</html>
