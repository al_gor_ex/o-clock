<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Создание аккаунта - O'Clock</title>
    <link rel="shortcut icon" type="image/png" href="assets/img/icons/favicon.ico">
    {literal}
        <script>
            alert('Аккаунт успешно создан!');
            document.location.href = 'login.php';
        </script>
    {/literal}
</head>
<body>
</body>
</html>
