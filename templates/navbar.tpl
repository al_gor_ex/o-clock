<head>
    <link rel="stylesheet" href="assets/css/navbar.css"/>
</head>
<body>
<div class="container-fluid">
    <div class="navbar navbar-expand-sm navbar-area d-flex">
        <ul class="navbar-nav font-weight-bold mr-auto">
            <li class="navbar-brand">
                <img class="brand" src="assets/img/navbar-icons/brand.png">
                O'clock
            </li>
        </ul>
        <ul class="navbar-nav font-weight-bold">
            {if $url != 'home.php'}
                <li class="nav-item px-2">
                    <a class="nav-link" href="home.php">
                        <img src="assets/img/navbar-icons/home.png">
                        Главная
                    </a>
                </li>
            {/if}
            {if $url != 'categories.php'}
                <li class="nav-item px-2">
                    <a class="nav-link" href="categories.php">
                        <img src="assets/img/navbar-icons/catalog.png">
                        Каталог
                    </a>
                </li>
            {/if}
            {if $url != 'search.php'}
                <li class="nav-item px-2">
                    <a class="nav-link" href="search.php">
                        <img src="assets/img/navbar-icons/search.png">
                        Поиск
                    </a>
                </li>
            {/if}
            {if $url != 'contacts.php'}
                <li class="nav-item px-2">
                    <a class="nav-link" href="contacts.php">
                        <img src="assets/img/navbar-icons/contact.png">
                        Контакты
                    </a>
                </li>
            {/if}
            {if $url != 'cart.php' && $role == BusinessLogic\Enums\Role::USER}
                <li class="nav-item px-2">
                    <a class="nav-link" href="cart.php">
                        <img src="assets/img/navbar-icons/cart.png">
                        Корзина
                    </a>
                </li>
            {/if}
            {if $role != BusinessLogic\Enums\Role::ANON}
                <li class="nav-item dropdown px-2 py-1 mr-5">
                    <a class="nav-link dropdown-toggle drop-btn">
                        {if $role == BusinessLogic\Enums\Role::USER}
                            {$userName}
                        {else}
                            Администратор
                        {/if}
                    </a>
                    <div class="dropdown-menu dropdown-content">
                        {if $url != 'orders.php'}
                        <a class="dropdown-item" href="orders.php">
                            <img src="assets/img/navbar-icons/orders.png">
                            Заказы
                        </a>
                        {/if}
                        {if $url != 'profile.php' && $role == BusinessLogic\Enums\Role::USER}
                        <a class="dropdown-item" href="profile.php">
                            <img src="assets/img/navbar-icons/profile.png">
                            Профиль
                        </a>
                        {/if}
                        {if $url != 'accounts.php' && $role == BusinessLogic\Enums\Role::ADMIN}
                        <a class="dropdown-item" href="accounts.php">
                            <img src="assets/img/navbar-icons/accounts.png">
                            Аккаунты
                        </a>
                        {/if}
                        <a class="dropdown-item" href="logout.php">
                            <img src="assets/img/navbar-icons/logout.png">
                            Выход
                        </a>
                    </div>
                </li>
            {/if}
            {if $url != 'login.php' && $role == BusinessLogic\Enums\Role::ANON}
            <li class="nav-item px-2">
                <a class="nav-link" href="login.php">
                    <img src="assets/img/navbar-icons/login.png">
                    Вход
                </a>
            </li>
            {/if}
        </ul>
    </div>
</div>
</body>
