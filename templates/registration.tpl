<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Регистрация - O'Clock</title>
    <link rel="shortcut icon" type="image/png" href="assets/img/icons/favicon.ico">
    <link rel="stylesheet" href="assets/css/bootstrap.css"/>
    <link rel="stylesheet" href="assets/css/registration.css"/>
    <script src="assets/js/jquery-3.5.1.js"></script>
    <script src="assets/js/registration.js"></script>
    <script src="assets/js/bootstrap.bundle.js"></script>
</head>
<body onload="onLoad()">
{include file="navbar.tpl"}
<div class="container registration-container h-100">
    <div class="row h-100 align-content-center justify-content-center">
        <div class="align-items-center">
            <h2 class="text-center mt-4">
                Регистрация
            </h2>
            <h6 class="text-center">
                Уже есть аккаунт?
                <a href="login.php">Войти</a>
            </h6>
        </div>
        <form action="create-user.php" method="post" name="credentials" class="w-100">
            <div class="form-group form-group-margin">
                Телефон
                <input class="form-control w-75" type="text" id="phone" name="phone" required/>
                <span class="error-message hidden">
                Некорректный номер телефона
            </span>
            </div>
            <div class="form-group form-group-margin">
                Имя
                <input class="form-control w-75" type="text" id="name" name="name" required/>
                <span class="error-message hidden">
                Имя не может быть длиннее 30 символов
            </span>
            </div>
            <div class="form-group form-group-margin">
            <span>
                <label class="checkbox-label">
                    <input type="checkbox" id="show-password" onchange="togglePasswordsVisibility()"/>
                    Показать пароли
                </label>
            </span>
            </div>
            <div class="form-group form-group-margin">
                Пароль
                <input class="form-control w-75" type="password" id="password" name="password" required/>
                <span class="error-message hidden">
                Введите минимум 8 символов и 2 цифры
            </span>
            </div>
            <div class="form-group form-group-margin">
                Повторите пароль
                <input class="form-control w-75" type="password" id="repeat-password" required/>
                <span class="error-message hidden">
                Пароли не совпадают
            </span>
            </div>
            <div class="form-group form-group-margin">
                <button class="btn btn-primary w-75" type="submit" id="btn-submit">
                    Отправить
                </button>
            </div>
        </form>
    </div>
</div>
</body>
</html>
