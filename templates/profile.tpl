<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Данные профиля - O'Clock</title>
    <link rel="shortcut icon" type="image/png" href="assets/img/icons/favicon.ico">
    <link rel="stylesheet" href="assets/css/bootstrap.css"/>
    <link rel="stylesheet" href="assets/css/profile.css"/>
    <script src="assets/js/jquery-3.5.1.js"></script>
    <script src="assets/js/profile.js"></script>
    <script src="assets/js/bootstrap.bundle.js"></script>
</head>
<body onload="onLoad()">
{include file="navbar.tpl"}
<div class="container content-container h-100">
    <div class="row h-100 align-content-center justify-content-center">
        <div class="align-items-center">
            <h3 class="text-center mt-4">
                Данные профиля
            </h3>
        </div>
        <form action="update-profile.php" method="post" name="credentials" class="w-100">
            <div class="form-group form-group-margin">
                Телефон
                <input class="form-control w-75" type="text" id="phone" name="phone" readonly
                       value="{$phone}"/>
            </div>
            <div class="form-group form-group-margin">
                Имя
                <input class="form-control w-75" type="text" id="name" name="name" required
                       value="{$userName}"/>
                <span class="error-message hidden">
                    Имя не может быть длиннее 30 символов
                </span>
            </div>
            <div class="form-group form-group-margin">
            <span>
                <label class="checkbox-label">
                    <input type="checkbox" id="show-password" onchange="togglePasswordsVisibility()"/>
                    Показать пароли
                </label>
            </span>
            </div>
            <div class="form-group form-group-margin">
                Старый пароль
                <input class="form-control w-75" type="password" id="old-password" name="old-password" required/>
            </div>
            <div class="form-group form-group-margin">
                Новый пароль
                <input class="form-control w-75" type="password" id="new-password" name="new-password"/>
                <span class="error-message hidden">
                Введите минимум 8 символов и 2 цифры
            </span>
            </div>
            <div class="form-group form-group-margin">
                Повторите новый пароль
                <input class="form-control w-75" type="password" id="repeat-password"/>
                <span class="error-message hidden">
                Пароли не совпадают
            </span>
            </div>
            <div class="d-flex flex-wrap justify-content-center form-group">
                <div>
                    <button class="btn btn-primary mr-3" type="submit" id="btn-submit">
                        Изменить
                    </button>
                </div>
                <div>
                    <a href="home.php">
                        <div class="btn btn-outline-danger">
                            Отмена
                        </div>
                    </a>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
