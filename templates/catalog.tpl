<!doctype html>
<html lang="ru" class="page-height">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Каталог - O'Clock</title>
    <link rel="shortcut icon" type="image/png" href="assets/img/icons/favicon.ico">
    <link rel="stylesheet" href="assets/css/bootstrap.css"/>
    <link rel="stylesheet" href="assets/css/jquery-ui-1.12.1.css"/>
    <link rel="stylesheet" href="assets/css/catalog.css"/>
    <script src="assets/js/jquery-3.5.1.js"></script>
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/catalog.js"></script>
    <script src="assets/js/jquery-ui-1.12.1.min.js"></script>
    <script src="assets/js/bootstrap.bundle.js"></script>
    <script src="assets/js/js.cookie.min.js"></script>
    <script src="assets/js/catalog-controls.js"></script>
    <script src="assets/js/catalog-network.js"></script>
</head>
<body class="page-height" onload="onLoad()">
{include file="navbar.tpl"}
<div class="row page-height">

    <div class="col-sm-3 filters-area pl-4 page-height">
        {if !empty($productsList)}
            <div class="d-flex justify-content-end mt-2 w-100">
                <img data-toggle="tooltip" title="Закрыть" src="assets/img/icons/delete.png"
                     class="control-icon" onclick="fadeFiltersArea();resizeProductsArea()">
            </div>
            <h6>Сортировка:</h6>
            <div class="form-group w-100">
                <select id="sort-by" class="w-100">
                    <option value="{BusinessLogic\Enums\SortingType::POPULARITY}"
                            {if $sortingType == BusinessLogic\Enums\SortingType::POPULARITY || is_null($sortingType)}
                                selected
                            {/if}
                    >
                        сначала популярные
                    </option>
                    <option value="{BusinessLogic\Enums\SortingType::NAME}"
                            {if $sortingType === BusinessLogic\Enums\SortingType::NAME}
                                selected
                            {/if}
                    >
                        по названию
                    </option>
                    <option value="{BusinessLogic\Enums\SortingType::PRICE_DESC}"
                            {if $sortingType === BusinessLogic\Enums\SortingType::PRICE_DESC}
                                selected
                            {/if}
                    >
                        сначала дороже
                    </option>
                    <option value="{BusinessLogic\Enums\SortingType::PRICE_ASC}"
                            {if $sortingType === BusinessLogic\Enums\SortingType::PRICE_ASC}
                                selected
                            {/if}
                    >
                        сначала дешевле
                    </option>
                    <option value="{BusinessLogic\Enums\SortingType::DISCOUNT}"
                            {if $sortingType === BusinessLogic\Enums\SortingType::DISCOUNT}
                                selected
                            {/if}
                    >
                        по размеру скидки
                    </option>
                </select>
            </div>
            <h6>Фильтры:</h6>
            <ul>
                <li>
                    <p class="mb-2">
                        Страна-производитель:
                    </p>
                    <div class="form-group w-100">
                        <select id="country-name" class="w-100">
                            <option value="all"
                                    {if is_null($countryEngName)}
                                        selected
                                    {/if}
                            >
                                Любая
                            </option>
                            {foreach from=$countriesList item="country"}
                                <option value="{$country.value}"
                                        {if $countryEngName == $country.value}
                                            selected
                                        {/if}
                                >
                                    {$country.name}
                                </option>
                            {/foreach}
                        </select>
                    </div>
                </li>
                <li>
                    <p class="mb-0">
                        Тип:
                    </p>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input"
                                   value="{BusinessLogic\Enums\MechanismType::MECHANIC}"
                                    {if strpos($mechanismType, BusinessLogic\Enums\MechanismType::MECHANIC) !== false}
                                        checked
                                    {/if}
                            >
                            Механические
                        </label>
                        <br>
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input"
                                   value="{BusinessLogic\Enums\MechanismType::ELECTRONIC}"
                                    {if strpos($mechanismType, BusinessLogic\Enums\MechanismType::ELECTRONIC) !== false}
                                        checked
                                    {/if}
                            >
                            Электронные
                        </label>
                    </div>
                </li>
                <li data-toggle="tooltip" title="Установите ценовой диапазон, перемещая квадратные ползунки">
                    <p class="mt-3">
                        Цена:
                    </p>
                    <div class="d-flex justify-content-between range-edge-prices">
                        <p id="min-price-label">
                            {$priceRange['lower']}
                        </p>
                        <p id="max-price-label">
                            {$priceRange['upper']}
                        </p>
                    </div>
                    <div id="price-range" class="mt-3"></div>
                    <div class="form-group mt-3">
                        От:
                        <input id="min-price" type="number" min="0" max="10000000"
                               value="{if is_null($minPrice)}{$priceRange['lower']}{else}{$minPrice}{/if}"
                               readonly/>
                        <br>
                        До:
                        <input id="max-price" class="mt-2" type="number" min="0" max="10000000"
                               value="{if is_null($maxPrice)}{$priceRange['upper']}{else}{$maxPrice}{/if}" readonly/>
                    </div>
                </li>
            </ul>
            <div class="text-center">
                <div class="btn btn-primary badge-pill w-100" onclick="onBtnApplyFiltersClick()">
                    Применить
                </div>
                <br>
                <div class="btn btn-outline-danger mt-3 badge-pill w-100" onclick="onBtnClearFiltersClick()">
                    Сбросить
                </div>
            </div>
        {/if}
    </div>


    <div class="col-sm-9 products-area">
        <div class="container-fluid mt-3 pt-5">
            {if $role == BusinessLogic\Enums\Role::ADMIN}
                <div class="d-flex justify-content-end mt-3">
                    <a href="new-edit-product.php?id=-1">
                        <button type="button" class="btn btn-success badge-pill">
                            <img src="assets/img/icons/plus.png">
                            Создать товар
                        </button>
                    </a>
                </div>
            {/if}
            {if empty($productsList)}
                <h5 class="text-center text-danger mt-3">
                    Товары отсутствуют
                </h5>
                <div class="d-flex justify-content-center">
                    <div class="mt-3 btn-primary badge-pill w-25 text-center" onclick="onBtnClearFiltersClick()">
                        Сбросить фильтры
                    </div>
                </div>
            {/if}
        </div>
        {if !empty($productsList)}
            {foreach from=$productsList item="row"}
                <div class="row mt-3 pl-2">
                    {foreach from=$row item="product"}
                        <div class="col product px-3">
                            {if $role == BusinessLogic\Enums\Role::ADMIN}
                                <div class="d-flex justify-content-end mb-1">
                                    <a href="new-edit-product.php?id={$product->id}">
                                        <img data-toggle="tooltip" title="Редактировать {$product->name}"
                                             src="assets/img/icons/edit.png" class="mr-2 control-icon"/>
                                    </a>
                                    <a onclick="deleteProduct({$product->id}, {$categoryId})">
                                        <img data-toggle="tooltip" title="Удалить {$product->name}"
                                             src="assets/img/icons/delete.png" class="mr-5 control-icon">
                                    </a>
                                </div>
                            {/if}
                            <p class="text-center">
                                <img class="product-img" src="{$product->photoPath}">
                            </p>
                            <h5 class="text-center">
                                {$product->name}
                            </h5>
                            <div class="mx-5 px-2">
                                {if $product->discountPercent > 0}
                                    <h6>
                                        <s>{$product->price}</s>
                                    </h6>
                                {/if}
                                <h4>
                                    {$product->finalPrice}
                                    {if $product->discountPercent > 0}
                                        <sup>
                                            <div class="badge badge-pill badge-primary">
                                                -{$product->discountPercent}%
                                            </div>
                                        </sup>
                                    {/if}
                                </h4>
                                <h6>
                                    {if $product->mechanismType == BusinessLogic\Enums\MechanismType::MECHANIC}
                                        механические
                                    {else}
                                        электронные
                                    {/if}
                                </h6>
                                <h6>
                                    {$product->countryName}
                                </h6>
                                {if $product->availableCount > 0}
                                    <h6>
                                        в наличии: {$product->availableCount} шт.
                                    </h6>
                                {else}
                                    <h6>
                                        нет в наличии
                                    </h6>
                                {/if}
                                {if $product->soldCount > 0}
                                    <h6>
                                        продано: {$product->soldCount} шт.
                                    </h6>
                                {/if}
                                {if $role != BusinessLogic\Enums\Role::ADMIN}
                                    {if $product->availableCount > 0}
                                        {if $role == BusinessLogic\Enums\Role::ANON}
                                            <a href="login.php">
                                                <div class="btn btn-primary w-100">
                                                    В корзину
                                                </div>
                                            </a>
                                        {/if}
                                        {if $role == BusinessLogic\Enums\Role::USER && $product->wasAddedToCart == false}
                                            <div class="btn btn-primary w-100" id="add{$product->id}"
                                                 onclick="notifyCart({$product->id}, 'add')">
                                                В корзину
                                            </div>
                                        {/if}
                                        {if $role == BusinessLogic\Enums\Role::USER && $product->wasAddedToCart}
                                            <div class="btn btn-danger w-100" id="remove{$product->id}"
                                                 onclick="notifyCart({$product->id}, 'remove')">
                                                Убрать из корзины
                                            </div>
                                        {/if}
                                    {else}
                                        <h6 class="text-danger text-center">
                                            Желаете заказать?
                                            <br>
                                            <a href="contacts.php" class="text-danger">
                                                <u>
                                                    <b>
                                                        Свяжитесь с нами
                                                    </b>
                                                </u>
                                            </a>
                                        </h6>
                                    {/if}
                                {/if}
                            </div>
                        </div>
                    {/foreach}
                </div>
            {/foreach}
            <div class="d-flex justify-content-center mt-3">
                {if $page != 1}
                    <a href="catalog.php?category-id={$categoryId}&page={$page-1}">
                        <img class="pagination-btn" src="assets/img/icons/pagination-left.png">
                    </a>
                {/if}
                <span class="h3 align-self-center text-primary mx-4">
                {$page} / {$pagesCount}
                </span>
                {if $page != $pagesCount}
                    <a href="catalog.php?category-id={$categoryId}&page={$page+1}">
                        <img class="pagination-btn" src="assets/img/icons/pagination-right.png">
                    </a>
                {/if}
            </div>
        {/if}
    </div>
</div>
</body>
</html>
