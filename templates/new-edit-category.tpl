<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/png" href="assets/img/icons/favicon.ico">
    <title>{if $isNew}Создание{else}Изменение{/if} категории - O'Clock</title>
    <link rel="stylesheet" href="assets/css/new-edit-category.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css"/>
    <script src="assets/js/jquery-3.5.1.js"></script>
    <script src="assets/js/bootstrap.bundle.js"></script>
</head>
<body>
<div class="container mt-4">
    <div class="text-center">
        <h2>
            {if $isNew}Создание{else}Изменение{/if} категории
        </h2>
    </div>

    <div class="d-flex flex-wrap justify-content-center">
        <form method="post" enctype="multipart/form-data"
              action="create-update-category.php?id={$id}" name="category-data" class="w-25">
            <div class="form-group w-100">
                Фото
                <input type="file" name="photo" {if $isNew}required{/if}>
                {if !$isNew}
                    <div class="small-text mt-2">
                        * Чтобы не изменять фото, оставьте поле "Фото" пустым
                    </div>
                {/if}
            </div>
            <div class="form-group w-100">
                Название
                <input class="form-control" type="text" name="name"
                       {if !$isNew}value="{$category->name}"{/if} required>
            </div>
            <div class="d-flex flex-wrap justify-content-center form-group">
                <div>
                    <button class="btn btn-primary mr-3" type="submit" id="btn-submit">
                        Сохранить
                    </button>
                </div>
                <div>
                    <a href="categories.php">
                        <div class="btn btn-outline-danger">
                            Отмена
                        </div>
                    </a>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
