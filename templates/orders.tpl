<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/png" href="assets/img/icons/favicon.ico">
    <title>Заказы - O'Clock</title>
    <link rel="stylesheet" href="assets/css/bootstrap.css"/>
    <link rel="stylesheet" href="assets/css/orders.css"/>
    <script src="assets/js/jquery-3.5.1.js"></script>
    <script src="assets/js/orders.js"></script>
    <script src="assets/js/bootstrap.bundle.js"></script>
</head>
<body onload="onLoad()">
{include file="navbar.tpl"}
<div class="container page-header">
    <div class="text-center">
        <h2>
            Заказы
        </h2>
    </div>
    {if empty($orders)}
        <div class="text-center mt-5 text-primary">
            <h5>
                Не было совершено ни одного заказа
            </h5>
        </div>
    {else}
        {if $role == BusinessLogic\Enums\Role::ADMIN}
            <div class="d-flex justify-content-end" id="btn-show-export-form"
                 onclick="onBtnShowFormClick()">
                <div class="btn btn-primary badge-pill text-white">
                <span>
                    <img class="export-icon" src="assets/img/icons/export-docx.png">
                </span>
                    Экспорт в DOCX
                </div>
            </div>
            <div class="d-flex flex-wrap justify-content-end align-items-center" id="export-form">
                <form name="report-dates" action="report.php" method="get" target="_blank" class="form-inline">
                    <div class="h6 mr-2">
                        Период: с
                    </div>
                    <div>
                        <input type="date" name="date-from" required>
                    </div>
                    <div class="h6 mx-2">
                        по
                    </div>
                    <div>
                        <input type="date" name="date-to" required>
                    </div>
                    <button class="btn btn-primary badge-pill ml-2">
                        Выполнить
                    </button>
                </form>
            </div>
        {/if}
        <div class="border-top border-dark mt-3">
            <!-- separation line -->
        </div>
        {foreach from=$orders item="order"}
            <div class="container-fluid w-75 mt-2">
                <h5 class="order-id">
                    # {$order->id}
                </h5>
                <div class="d-flex flex-wrap mt-2">
                    <div class="mr-4">
                        {$order->createdAt}
                    </div>
                    {if $role == BusinessLogic\Enums\Role::ADMIN}
                        <div class="mr-4">
                            {$order->userPhone}
                        </div>
                        <div class="mr-4">
                            {$order->userName}
                        </div>
                    {/if}
                    <div class="mr-4 font-weight-bold">
                        {$order->sum} Р
                    </div>
                </div>
                <h5 class="mt-2">
                    Товары:
                </h5>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>
                            &numero;
                        </th>
                        <th>
                            Название
                        </th>
                        <th>
                            Цена (с учётом скидки)
                        </th>
                        <th>
                            Кол-во (шт.)
                        </th>
                        <th>
                            Стоимость
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach from=$order->products item="product" name="products"}
                        <tr>
                            <td>
                                {$smarty.foreach.products.index + 1}
                            </td>
                            <td>
                                {$product->name}
                            </td>
                            <td>
                                {$product->finalPrice} Р
                            </td>
                            <td>
                                {$product->count}
                            </td>
                            <td>
                                {$product->sum} Р
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
            <div class="border-top border-dark mt-3">
                <!-- separation line -->
            </div>
        {/foreach}
        <div class="d-flex justify-content-center mt-3">
            {if $page != 1}
                <a href="orders.php?page={$page-1}">
                    <img class="pagination-btn" src="assets/img/icons/pagination-left.png">
                </a>
            {/if}
            <span class="h3 align-self-center text-primary mx-4">
                {$page} / {$pagesCount}
                </span>
            {if $page != $pagesCount}
                <a href="orders.php?page={$page+1}">
                    <img class="pagination-btn" src="assets/img/icons/pagination-right.png">
                </a>
            {/if}
        </div>
    {/if}
</div>
</body>
</html>
