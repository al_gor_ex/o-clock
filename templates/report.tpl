<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/png" href="assets/img/icons/favicon.ico">
    <title>Отчёт - O'Clock</title>
</head>
<body>
    <object data="storage/reports/{$fileName}" style="width: 100%; height: 95vh"></object>
</body>
</html>
