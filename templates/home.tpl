<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Главная - O'Clock</title>
    <link rel="shortcut icon" type="image/png" href="assets/img/icons/favicon.ico">
    <link rel="stylesheet" href="assets/css/bootstrap.css"/>
    <link rel="stylesheet" href="assets/css/home.css"/>
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css"/>
    <link rel="stylesheet" type="text/css" href="assets/css/slick-theme.css"/>
    <script src="assets/js/jquery-3.5.1.js"></script>
    <script src="assets/js/bootstrap.bundle.js"></script>
    <script src="assets/js/home.js"></script>
    <script src="assets/js/slick.min.js"></script>
    <script src="assets/js/home-slider.js"></script>
</head>
<body onload="onLoad()">
{include file="navbar.tpl"}
<div class="auto-slider">
    <div><img class="slider-img" src="assets/img/home-slides/1.jpg"></div>
    <div><img class="slider-img" src="assets/img/home-slides/2.jpg"></div>
    <div><img class="slider-img" src="assets/img/home-slides/3.jpg"></div>
    <div><img class="slider-img" src="assets/img/home-slides/4.jpg"></div>
    <div><img class="slider-img" src="assets/img/home-slides/5.jpg"></div>
    <div><img class="slider-img" src="assets/img/home-slides/6.jpg"></div>
    <div><img class="slider-img" src="assets/img/home-slides/7.jpg"></div>
    <div><img class="slider-img" src="assets/img/home-slides/8.png"></div>
    <div><img class="slider-img" src="assets/img/home-slides/9.png"></div>
    <div><img class="slider-img" src="assets/img/home-slides/10.jpg"></div>
    <div><img class="slider-img" src="assets/img/home-slides/11.jpg"></div>
    <div><img class="slider-img" src="assets/img/home-slides/12.jpg"></div>
</div>
<div class="container mt-4">
    <p class="welcome-message">
        Мы рады видеть Вас на сайте O'Clock! С 1970 года мы непрерывно совершенствуем
        наши технологии и сервис, чтобы дарить своим покупателям точность и уверенность
        в качестве нашей продукции
    </p>
    <p class="advantages-header font-weight-bold my-5">
        Особенности O'Clock:
    </p>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col advantage py-5">Надёжность изделий</div>
        <div class="col advantage py-5">Приятные бонусы и скидки</div>
        <div class="col advantage py-5">Бесплатная доставка</div>
    </div>
    <div class="row">
        <div class="col advantage py-5">Высокоточный механизм</div>
        <div class="col advantage advantage--highlighted py-5">Технологии, проверенные временем</div>
        <div class="col advantage py-5">Длительная гарантия</div>
    </div>
    <div class="row">
        <div class="col advantage py-5">Сертификаты к товарам</div>
        <div class="col advantage py-5">Сервисные центры в 30 городах России</div>
        <div class="col advantage py-5">Возврат и обмен</div>
    </div>
</div>
<div class="container mt-4">
    <div class="row">
        <div class="col question">
            Желаете ознакомиться с изделиями?
        </div>
        <div class="col">
            <a href="categories.php" class="link w-100">
                <div class="btn btn-outline-primary w-100">
                    Каталог
                </div>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col question">
            Ищете конкретную модель?
        </div>
        <div class="col">
            <a href="search.php" class="link w-100">
                <div class="btn btn-outline-primary w-100">
                    Поиск
                </div>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col question">
            Намерены посетить магазин лично?
        </div>
        <div class="col">
            <a href="contacts.php" class="link w-100">
                <div class="btn btn-outline-primary w-100">
                    Адреса магазинов
                </div>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col question">
            Остались вопросы?
        </div>
        <div class="col">
            <a href="contacts.php" class="link w-100">
                <div class="btn btn-outline-primary w-100">
                    Горячая линия
                </div>
            </a>
        </div>
    </div>
</div>
{include file="footer.tpl"}
</body>
</html>
