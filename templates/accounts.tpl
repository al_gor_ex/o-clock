<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/png" href="assets/img/icons/favicon.ico">
    <title>Аккаунты - O'Clock</title>
    <link rel="stylesheet" href="assets/css/bootstrap.css"/>
    <link rel="stylesheet" href="assets/css/accounts.css"/>
    <script src="assets/js/jquery-3.5.1.js"></script>
    <script src="assets/js/bootstrap.bundle.js"></script>
    <script src="assets/js/enable-tooltips.js"></script>
    <script src="assets/js/accounts.js"></script>
</head>
<body>
{include file="navbar.tpl"}
<div class="container page-header">
    <div class="text-center mb-4">
        <h2>
            Аккаунты
        </h2>
    </div>
    {if !empty($accountsList)}
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>
                    Телефон
                </th>
                <th>
                    Имя
                </th>
                <th>
                    Дата регистрации
                </th>
                <th>
                    Кол-во заказов
                </th>
                <th>
                    Средний чек
                </th>
                <th>
                    Дата последнего заказа
                </th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            {foreach from=$accountsList item="account"}
                <tr>
                    <td>
                        {$account->phone}
                    </td>
                    <td>
                        {$account->name}
                    </td>
                    <td>
                        {$account->registrationDate}
                    </td>
                    {$hasOrders = ($account->ordersCount > 0)}
                    <td>
                        {if $hasOrders}
                            {$account->ordersCount}
                        {else}
                            &mdash;
                        {/if}
                    </td>
                    <td>
                        {if $hasOrders}
                            {$account->averageSum} Р
                        {else}
                            &mdash;
                        {/if}
                    </td>
                    <td>
                        {if $hasOrders}
                            {$account->lastOrderDate}
                        {else}
                            &mdash;
                        {/if}
                    </td>
                    <td>
                        <img data-toggle="tooltip" onclick="deleteAccount('{$account->phone}')"
                             title="Удалить пользователя {$account->name}"
                             src="assets/img/icons/delete.png" class="mr-5 control-icon">
                    </td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    {/if}
</div>
</body>
</html>
