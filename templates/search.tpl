<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Поиск - O'Clock</title>
    <link rel="shortcut icon" type="image/png" href="assets/img/icons/favicon.ico">
    <link rel="stylesheet" href="assets/css/bootstrap.css"/>
    <link rel="stylesheet" href="assets/css/jquery-ui-1.12.1.css"/>
    <link rel="stylesheet" href="assets/css/search.css"/>
    <script src="assets/js/jquery-3.5.1.js"></script>
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/enable-tooltips.js"></script>
    <script src="assets/js/jquery-ui-1.12.1.min.js"></script>
    <script src="assets/js/bootstrap.bundle.js"></script>
    <script src="assets/js/js.cookie.min.js"></script>
    <script src="assets/js/catalog-network.js"></script>
    <script src="assets/js/search.js"></script>
</head>
<body>
{include file="navbar.tpl"}
<div class="container page-header">
    <div class="d-flex justify-content-center flex-wrap">
        <div class="align-self-center">
            Категория:
        </div>
        <form class="form-inline" action="search.php" method="get">
            <select name="category-id" class="ml-2 form-control">
                <option value="all"
                        {if is_null($categoryId)}
                            selected
                        {/if}
                >
                    Любая
                </option>
                {foreach from=$categories item="category"}
                    <option value="{$category->id}"
                            {if $categoryId == $category->id}
                                selected
                            {/if}
                    >
                        {$category->name}
                    </option>
                {/foreach}
            </select>
            <input name="phrase" class="product-name ml-2 form-control"
                   placeholder="название товара" value="{$phrase}" required/>
            <button type="submit" class="btn btn-primary badge-pill ml-2">
                <img src="assets/img/icons/search.png">
                Искать
            </button>
        </form>
    </div>
    {if empty($productsList) && !is_null($phrase)}
        <h5 class="text-center mt-5">
            По Вашему запросу ничего не найдено
        </h5>
        {if $role != BusinessLogic\Enums\Role::ADMIN}
            <h5 class="text-center text-danger mt-2">
                Не нашли то, что иcкали?
                <a href="contacts.php" class="text-danger contact-us-link">
                    Свяжитесь с нами
                </a>
            </h5>
        {/if}
    {/if}


    {if !empty($productsList)}
        <h6 class="ml-5 mt-3">
            Найдено результатов: {count($productsList)}
        </h6>
        {for $i=0 to ceil(count($productsList) / 3) - 1}
            <div class="row mt-3 pl-2">
                {for $j=0 to (3 - 1)}
                    {*to prevent index out of bounds exception*}
                    {if $i * 3 + $j >= count($productsList)}
                        {break}
                    {/if}
                    {$product=$productsList[$i * 3 + $j]}
                    <div class="col product px-3">
                        {if $role == BusinessLogic\Enums\Role::ADMIN}
                            <div class="d-flex justify-content-end mb-1">
                                <a href="new-edit-product.php?id={$product->id}">
                                    <img data-toggle="tooltip" title="Редактировать {$product->name}"
                                         src="assets/img/icons/edit.png" class="mr-2 control-icon"/>
                                </a>
                                <a onclick="deleteProduct({$product->id}, -1)">
                                    <img data-toggle="tooltip" title="Удалить {$product->name}"
                                         src="assets/img/icons/delete.png" class="mr-5 control-icon">
                                </a>
                            </div>
                        {/if}
                        <p class="text-center">
                            <img class="product-img" src="{$product->photoPath}">
                        </p>
                        <h5 class="text-center">
                            {$product->name}
                        </h5>
                        <div class="mx-5 px-2">
                            {if $product->discountPercent > 0}
                                <h6>
                                    <s>{$product->price}</s>
                                </h6>
                            {/if}
                            <h4>
                                {$product->finalPrice}
                                {if $product->discountPercent > 0}
                                    <sup>
                                        <div class="badge badge-pill badge-primary">
                                            -{$product->discountPercent}%
                                        </div>
                                    </sup>
                                {/if}
                            </h4>
                            <h6>
                                {if $product->mechanismType == BusinessLogic\Enums\MechanismType::MECHANIC}
                                    механические
                                {else}
                                    электронные
                                {/if}
                            </h6>
                            <h6>
                                {$product->countryName}
                            </h6>
                            {if $product->availableCount > 0}
                                <h6>
                                    в наличии: {$product->availableCount} шт.
                                </h6>
                            {else}
                                <h6>
                                    нет в наличии
                                </h6>
                            {/if}
                            {if $product->soldCount > 0}
                                <h6>
                                    продано: {$product->soldCount} шт.
                                </h6>
                            {/if}
                            {if $role != BusinessLogic\Enums\Role::ADMIN}
                                {if $product->availableCount > 0}
                                    {if $role == BusinessLogic\Enums\Role::ANON}
                                        <a href="login.php">
                                            <div class="btn btn-primary w-100">
                                                В корзину
                                            </div>
                                        </a>
                                    {/if}
                                    {if $role == BusinessLogic\Enums\Role::USER && $product->wasAddedToCart == false}
                                        <div class="btn btn-primary w-100" id="add{$product->id}"
                                             onclick="notifyCart({$product->id}, 'add')">
                                            В корзину
                                        </div>
                                    {/if}
                                    {if $role == BusinessLogic\Enums\Role::USER && $product->wasAddedToCart}
                                        <div class="btn btn-danger w-100" id="remove{$product->id}"
                                             onclick="notifyCart({$product->id}, 'remove')">
                                            Убрать из корзины
                                        </div>
                                    {/if}
                                {else}
                                    <h6 class="text-danger text-center">
                                        Желаете заказать?
                                        <br>
                                        <a href="contacts.php" class="text-danger">
                                            <u>
                                                <b>
                                                    Свяжитесь с нами
                                                </b>
                                            </u>
                                        </a>
                                    </h6>
                                {/if}
                            {/if}
                        </div>
                    </div>
                {/for}
            </div>
        {/for}
    {/if}
</div>
</body>
</html>
