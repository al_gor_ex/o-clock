<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/png" href="assets/img/icons/favicon.ico">
    <title>Корзина - O'Clock</title>
    <link rel="stylesheet" href="assets/css/bootstrap.css"/>
    <link rel="stylesheet" href="assets/css/cart.css"/>
    <script src="assets/js/jquery-3.5.1.js"></script>
    <script src="assets/js/bootstrap.bundle.js"></script>
    <script src="assets/js/enable-tooltips.js"></script>
    <script src="assets/js/cart.js"></script>
</head>
<body>
{include file="navbar.tpl"}
<div class="container mt-4">
    <div class="text-center page-header">
        <h2>
            Корзина
        </h2>
    </div>
    {if $anyProducts == false}
        <div class="text-center text-primary mt-5">
            <h5>
                Здесь пока пусто. Просмотрите
                <a href="categories.php" class="link-underlined">каталог</a>
                или воспользуйтесь
                <a href="search.php" class="link-underlined">поиском</a>,
                чтобы найти и добавить товары
            </h5>
        </div>
    {else}
        <div class="row mt-5">
            <div class="col-8">
                <h5>
                    Товаров в корзине: &nbsp; {$productsList->productsCount}
                </h5>
                <h4>
                    <span class="text-primary mr-2">
                        ИТОГО:
                    </span>
                    {if $productsList->discountSum > 0}
                        <span class="old-price text-secondary mr-2">
                            {$productsList->price}
                        </span>
                        -
                        <span class="text-danger">
                            {$productsList->discountSum}
                        </span>
                        =
                    {/if}
                    <span class="text-primary border border-primary final-price">
                        {$productsList->finalPrice} Р
                    </span>
                </h4>
            </div>
            <div class="col-4 text-center">
                <a href="checkout.php" class="mt-3 btn btn-primary btn-checkout badge-pill">
                    <span class="h5">Оформить заказ</span>
                </a>
            </div>
        </div>
        <div class="mt-3">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>
                        №
                    </th>
                    <th>
                        Название
                    </th>
                    <th>
                        Фото
                    </th>
                    <th>
                        Цена
                    </th>
                    <th>
                        Кол-во
                    </th>
                    <th>
                        Стоимость
                    </th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {foreach from=$productsList->products item=product name=products}
                    {$foreachIndex = $smarty.foreach.products.index}
                    <tr id="row{$foreachIndex + 1}">
                        <td>
                            {$foreachIndex + 1}
                        </td>
                        <td>
                            {$product->name}
                        </td>
                        <td class="text-center">
                            <img src="{$product->photoPath}" class="product-photo img-fluid">
                        </td>
                        <td>
                            {if $product->discountPercent > 0}
                                <div>
                                    <span class="old-price mr-2">
                                        {$product->price}
                                    </span>
                                    <span class="badge badge-danger badge-pill text-white">
                                        -{$product->discountPercent}%
                                    </span>
                                </div>
                            {/if}
                            <div class="h5">
                                {$product->finalPrice} Р
                            </div>
                        </td>
                        <td class="text-center">
                            <input id="productCount{$product->id}" data-toggle="tooltip" title="Изменить количество"
                                   class="w-50" type="number" value="{$product->count}"
                                   min="1" max="{Configs\CartConfig::EACH_PRODUCT_MAX_COUNT}" onkeydown="return false"
                                   onchange="onProductCountChange({$product->id}, '{$userPhone}')"/>
                        </td>
                        <td>
                            <div class="h4">
                                {$product->finalSum} Р
                            </div>
                        </td>
                        <td>
                            {$redirectId = ''}
                            {if $productsList->productsCount > 1}
                                {if $foreachIndex == 0}
                                    {$redirectId = "row1"}
                                {else}
                                    {$redirectId = "row$foreachIndex"}
                                {/if}
                            {/if}
                            <img data-toggle="tooltip" title="Убрать {$product->name} из корзины"
                                 src="assets/img/icons/delete.png" class="mr-5 control-icon"
                                 onclick="confirmAndRedirectTo('{$product->name}',
                                         'remove-from-cart.php?redirect-id={$redirectId}&product={$product->id}&phone={urlencode($userPhone)}')">
                        </td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>
    {/if}
</div>
{if $anyProducts}
    {include file="footer.tpl"}
{/if}
</body>
</html>
