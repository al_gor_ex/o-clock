<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Контакты - O'Clock</title>
    <link rel="shortcut icon" type="image/png" href="assets/img/icons/favicon.ico">
    <link rel="stylesheet" href="assets/css/bootstrap.css"/>
    <link rel="stylesheet" href="assets/css/contacts.css"/>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey={$apiKey}"
            type="text/javascript">
    </script>
    <script src="assets/js/contacts-yandex-map.js" type="text/javascript"></script>
</head>
<body>
{include file="navbar.tpl"}
<h2 class="text-center page-header">
    Контакты
</h2>
<div class="container">
    <h5>
        Единая горячая линия: <span class="font-weight-bold ml-3 bg-yellow">+7-123-456-78-90</span>
    </h5>
    <h5 class="mt-5">
        Салоны часов:
    </h5>
</div>
<div class="container-fluid px-5">
    <div class="row">
        <div class="col px-5 py-2 mr-4 shop-block">
            <div class="row text-center">
                <div class="col">
                    <img class="img-thumbnail store-photo" src="assets/img/store-photos/1.png">
                    <p class="text-center">
                        Ул. Снежная, 8а
                    </p>
                </div>
            </div>
            <div class="row pl-5">
                <p>
                    Пн-Пт: 8<sup>00</sup> - 18<sup>00</sup>
                    <br>
                    Сб-Вс: 10<sup>30</sup>- 20<sup>30</sup>
                </p>
            </div>
            <div class="row pl-5">
                <p>
                    Трамваи:
                    <span class="badge badge-danger">3</span>,
                    <span class="badge badge-warning">6</span>
                    <br>
                    Автобус:
                    <span class="badge badge-pill badge-primary">9a</span>
                </p>
            </div>
        </div>
        <div class="col px-5 py-2 mr-4 shop-block">
            <div class="row text-center">
                <div class="col">
                    <img class="img-thumbnail store-photo" src="assets/img/store-photos/2.png">
                    <p class="text-center">
                        Ул. Паскаля, 23
                    </p>
                </div>
            </div>
            <div class="row pl-5">
                <p>
                    Ежедневно: 10<sup>00</sup> - 17<sup>30</sup>
                </p>
            </div>
            <div class="row pl-5">
                <p>
                    Автобусы:
                    <span class="badge badge-pill badge-primary">4</span>
                    <span class="badge badge-pill badge-success">10</span>
                    <span class="badge badge-pill badge-secondary">21</span>
                    <br>
                    Автобусы:
                    <span class="badge badge-info">1</span>
                    <span class="badge badge-warning">7</span>
                </p>
            </div>
        </div>
        <div class="col px-5 py-2 shop-block">
            <div class="row text-center">
                <div class="col">
                    <img class="img-thumbnail store-photo" src="assets/img/store-photos/3.png">
                    <p class="text-center">
                        Пр-д. Неоновый, 3
                    </p>
                </div>
            </div>
            <div class="row pl-5">
                <p>
                    Ср-Вс: 9<sup>00</sup> - 20<sup>00</sup>
                </p>
            </div>
            <div class="row pl-5">
                <p>
                    Автобусы:
                    <span class="badge badge-pill badge-warning">50</span>
                    <span class="badge badge-pill badge-success">78</span>
                    <span class="badge badge-pill badge-danger">82</span>
                    <span class="badge badge-pill badge-info">93</span>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="container mt-3">
    <h5>
        На карте:
    </h5>
    <div class="d-flex justify-content-center">
        <div id="map"></div>
    </div>
</div>
{include file="footer.tpl"}
</body>
</html>
