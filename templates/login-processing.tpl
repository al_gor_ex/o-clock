<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Вход в аккаунт - O'Clock</title>
    <link rel="shortcut icon" type="image/png" href="assets/img/icons/favicon.ico">
    {if $result == true}
    {literal}
        <script>
            document.location.href = 'cart.php';
        </script>
    {/literal}
    {else}
    {literal}
        <script>
            alert('Неверный номер телефона или пароль');
            document.location.href = 'login.php';
        </script>
    {/literal}
    {/if}
</head>
<body>
</body>
</html>
