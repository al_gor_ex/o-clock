<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Каталог - O'Clock</title>
    <link rel="shortcut icon" type="image/png" href="assets/img/icons/favicon.ico">
    <link rel="stylesheet" href="assets/css/bootstrap.css"/>
    <link rel="stylesheet" href="assets/css/categories.css"/>
    <script src="assets/js/jquery-3.5.1.js"></script>
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/bootstrap.bundle.js"></script>
    <script src="assets/js/enable-tooltips.js"></script>
    <script src="assets/js/categories.js"></script>
</head>
<body>
{include file="navbar.tpl"}
<h2 class="text-center page-header">
    Категории
</h2>
<div class="container">
    <div class="d-flex justify-content-end">
        {if $role == BusinessLogic\Enums\Role::ADMIN}
            <a href="new-edit-category.php?id=-1">
                <button type="button" class="btn btn-success badge-pill">
                    <img src="assets/img/icons/plus.png">
                    Создать категорию
                </button>
            </a>
        {/if}
    </div>
    {if empty($categories)}
        {if $role == BusinessLogic\Enums\Role::ADMIN}
            <h5 class="text-center text-danger mt-3">
                Категории отсутствуют
            </h5>
        {else}
            <h5 class="text-center mt-3">
                На сайте ведутся технические работы. Приносим извинения за неудобства и ждём Вас снова!
            </h5>
        {/if}
    {else}
        {foreach from=$categories item="row"}
            <div class="row mt-5">
                {foreach from=$row item="category"}
                    <div class="col text-center">
                        {if $role == BusinessLogic\Enums\Role::ADMIN}
                        <div class="d-flex justify-content-end mb-1">
                            <a href="new-edit-category.php?id={$category->id}">
                                <img data-toggle="tooltip" title="Редактировать {$category->name}"
                                     src="assets/img/icons/edit.png" class="mr-2 control-icon"/>
                            </a>
                            <a onclick="deleteCategory({$category->id})">
                                <img data-toggle="tooltip" title="Удалить {$category->name}"
                                     src="assets/img/icons/delete.png" class="mr-5 control-icon">
                            </a>
                        </div>
                        {/if}
                        <a href="catalog.php?category-id={$category->id}&page=1">
                            <img class="category-img" src="{$category->photoPath}">
                            <h5 class="mt-2">
                                {$category->name}
                            </h5>
                        </a>
                    </div>
                {/foreach}
            </div>
        {/foreach}
    {/if}
</div>
{include file="footer.tpl"}
</body>
</html>
