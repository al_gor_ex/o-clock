<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/png" href="assets/img/icons/favicon.ico">
    <title>Оформление заказа - O'Clock</title>
    <link rel="stylesheet" href="assets/css/bootstrap.css"/>
    <script src="assets/js/jquery-3.5.1.js"></script>
    <script src="assets/js/bootstrap.bundle.js"></script>
</head>
<body>
{if $result->isSuccessful}
    <div class="container-fluid mt-5 pt-5">
        <div class="text-center">
            <h5 class="text-primary">
                Заказ успешно оформлен!
            </h5>
            <a href="orders.php">
                <div class="btn btn-primary badge-pill mt-4">
                    Перейти в раздел "Заказы"
                </div>
            </a>
        </div>
    </div>
{else}
    <div class="container-fluid mt-5 pt-5">
        <h5 class="text-center text-danger">
            Не удалось оформить заказ из-за недостатка на складе следующих товаров:
        </h5>
        <div class="d-flex justify-content-center">
            <div class="d-flex flex-column mt-4">
                {foreach from=$result->unavailableNames item=name}
                <ul class="list-group">
                    <li class="list-group-item list-group-item-action">
                        {$name}
                    </li>
                </ul>
                {/foreach}
            </div>
        </div>
        <div class="text-center">
            <a href="cart.php">
                <div class="btn btn-danger badge-pill mt-4 mb-5">
                    Вернуться в корзину
                </div>
            </a>
        </div>
    </div>
{/if}
</body>
</html>
