<!doctype html>
<html lang="ru" class="full-screen-height">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/png" href="assets/img/icons/favicon.ico">
    <title>Вход - O'Clock</title>
    <link rel="stylesheet" href="assets/css/bootstrap.css"/>
    <link rel="stylesheet" href="assets/css/login.css"/>
    <script src="assets/js/jquery-3.5.1.js"></script>
    <script src="assets/js/login.js"></script>
    <script src="assets/js/bootstrap.bundle.js"></script>
</head>
<body class="full-screen-height">
{include file="navbar.tpl"}
<div class="container login-container h-100">
    <div class="row h-100 align-content-center justify-content-center">
        <div class="align-items-center">
            <h2 class="text-center">
                Вход
            </h2>
            <h6 class="text-center">
                Нет аккаунта?
                <a href="registration.php">Зарегистрироваться</a>
            </h6>
        </div>
        <form action="login-processing.php" method="post" name="credentials" class="w-100">
            <div class="form-group form-group-margin">
                Телефон
                <input class="form-control w-75" type="text" id="phone" name="phone" required/>
            </div>
            <div class="form-group form-group-margin">
            <span>
                <label class="checkbox-label">
                    <input type="checkbox" id="show-password" onchange="togglePasswordVisibility()"/>
                    Показать пароль
                </label>
            </span>
            </div>
            <div class="form-group form-group-margin">
                Пароль
                <input class="form-control w-75" type="password" id="password" name="password" required/>
            </div>
            <div class="form-group form-group-margin">
                <button class="btn btn-primary w-75" type="submit" id="btn-submit">
                    Отправить
                </button>
            </div>
        </form>
    </div>
</div>
</body>
</html>
