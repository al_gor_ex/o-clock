<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/png" href="assets/img/icons/favicon.ico">
    <title>{if $isNew}Создание{else}Изменение{/if} товара - O'Clock</title>
    <link rel="stylesheet" href="assets/css/new-edit-product.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css"/>
    <script src="assets/js/jquery-3.5.1.js"></script>
    <script src="assets/js/bootstrap.bundle.js"></script>
    <script src="assets/js/new-edit-product.js"></script>
</head>
<body>
<div class="container mt-4">
    <div class="text-center">
        <h2>
            {if $isNew}Создание{else}Изменение{/if} товара
        </h2>
    </div>

    <div class="d-flex flex-wrap justify-content-center">
        <form method="post" enctype="multipart/form-data"
              action="create-update-product.php?id={$id}" name="product-data" class="w-25">
            <div class="form-group w-100">
                Фото
                <div class="mt-4 mb-4"></div>
                <input type="file" name="photo" {if $isNew}required{/if}>
                {if !$isNew}
                    <div class="small-text mt-4">
                        * Чтобы не изменять фото, оставьте поле "Фото" пустым
                    </div>
                {/if}
            </div>
            <div class="form-group w-100">
                Категория
                <select name="category_id" class="form-control" required>
                    {foreach from=$categoriesList key="i" item="category"}
                        <option value="{$category->id}"
                                {if $isNew && $i == 0 ||
                                !$isNew && $product->categoryId == $category->id}
                                    selected
                                {/if}
                        >
                            {$category->name}
                        </option>
                    {/foreach}
                </select>
            </div>
            <div class="form-group w-100">
                Название
                <input class="form-control" type="text" name="name"
                       {if !$isNew}value="{$product->name}"{/if} required>
            </div>
            <div class="form-group w-100">
                Цена, P
                <input class="form-control" type="number" name="price"
                       value="{if !$isNew}{$product->price}{else}0{/if}" min="0"
                       onchange="onPriceOrPercentChange()" required>
            </div>
            <div class="form-check w-100">
                <label class="form-check-label">
                    <input type="checkbox" class="form-check-input" id="enable-discount"
                           onchange="onDiscountToggle()">
                    Скидка
                </label>
            </div>
            <div id="discount-controls" class="form-group w-100 mt-2 hidden">
                <input name="discount_percent" type="number"
                       value="{if $isNew}0{else}{$product->discountPercent}{/if}" min="0"
                       max="99"
                       onchange="onPriceOrPercentChange()">
                %
                <span id="final-price" class="ml-4">
                    {if !$isNew}
                        {$product->price * (1 - $product->discountPercent / 100)}
                    {/if}
                </span>
                Р
            </div>
            <div class="w-100">
                Тип:
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="mechanism_type"
                               value="{BusinessLogic\Enums\MechanismType::MECHANIC}"
                                {if $isNew ||
                                    !$isNew && $product->mechanismType == BusinessLogic\Enums\MechanismType::MECHANIC}
                                    checked
                                {/if}>
                        Механические
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="mechanism_type"
                               value="{BusinessLogic\Enums\MechanismType::ELECTRONIC}"
                                {if !$isNew && $product->mechanismType == BusinessLogic\Enums\MechanismType::ELECTRONIC}
                                    checked
                                {/if}
                        >
                        Электронные
                    </label>
                </div>
            </div>
            <div class="form-group w-100 mt-3">
                Страна-производитель
                <input class="form-control" type="text" name="country_name"
                        {if !$isNew}
                            value="{$product->countryName}"
                        {/if}
                       required>
            </div>
            <div class="form-group w-100">
                В наличии:
                <input name="available_count" type="number"
                       value="{if $isNew}0{else}{$product->availableCount}{/if}" min="0"
                       max="1000" required>
                шт
            </div>
            <div class="d-flex flex-wrap justify-content-center form-group">
                <div>
                    <button class="btn btn-primary mr-3" type="submit" id="btn-submit">
                        Сохранить
                    </button>
                </div>
                <div>
                    <a href="catalog.php?category-id={if $isNew}1{else}{$product->categoryId}{/if}&page=1">
                        <div class="btn btn-outline-danger">
                            Отмена
                        </div>
                    </a>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
