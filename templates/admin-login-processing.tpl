<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Вход в аккаунт - O'Clock</title>
    <link rel="shortcut icon" type="image/png" href="assets/img/icons/favicon.ico">
    {if $result == true}
    {literal}
        <script>
            document.location.href = 'orders.php';
        </script>
    {/literal}
    {else}
    {literal}
        <script>
            alert('Неверные данные');
            document.location.href = 'admin-login.html';
        </script>
    {/literal}
    {/if}
</head>
<body>
</body>
</html>
