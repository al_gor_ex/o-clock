<?php

use BusinessLogic\Enums\Role;
use BusinessLogic\Guards\RoleGuard;
use BusinessLogic\Services\UserService;

require_once __DIR__ . '/vendor/autoload.php';

(new RoleGuard())->letIn(Role::ADMIN);

if (!isset($_GET['phone'])) {
    die('Неверные параметры запроса');
}
$phone = $_GET['phone'];

$userService = new UserService();
$userService->deleteUser($phone);

header('Location: accounts.php');
